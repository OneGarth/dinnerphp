<?php
require_once 'functions.php';

if(!isLogin()) {
    alert("登录超时或未登录，请重新登录");
    goLogin();
    exit;
}

if(!isset($_GET['id'])) {
    alert("没有此订单");
    goBack();
    exit;
}

$order = orderGetById($_GET['id']);

if($order == null) {
    alert("无此订单");
    goBack();
    exit;
}

if($order['user_id'] != $_SESSION['userInfo']['_id']) {
    alert("这不是你的订单，你无权访问");
    goBack();
    exit;
}

?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>订单详情</title>
    <script src="static/js/jquery-1.7.1.js"></script>
    <script src="static/js/jquery.validate.min.js"></script>
    <script src="static/js/jquery.gritter.min.js"></script>
    <script src="static/js/myorder.js"></script>
    <link href="static/css/basic.css" rel="stylesheet">
    <link href="static/css/common.css" rel="stylesheet">
    <link href="static/css/restaurant.css" rel="stylesheet">
    <link href="static/css/jquery-ui.min.css" rel="stylesheet">
    <link href="static/css/jquery.ui.theme.css" rel="stylesheet">
    <link href="static/css/account_login.css" rel="stylesheet">
    <link href="static/css/jquery.gritter.css" rel="stylesheet">
</head>
<body>

<style>
    .mytable {
        border: dashed #B2D460 1px;
    }
    .mytable tr td {
        vertical-align:middle;
        text-align:center;
        padding: 10px;
    }
    .mycenter {
        margin: 0 auto;
    }
    .textcenter {
        text-align: center;
    }
</style>

<?php include 'header.php'; ?>

<div class="page-wrap">
    <div class="inner-wrap">

        <div class="page-body block">
            <h2 class="title1 padding20 text-center">订单详情</h2>
            <div style="padding: 20px;">
                <?php
                    switch($order['status']) {
                        case 0:
                            $status = '处理中';
                            break;
                        case 1:
                            $status = '配送中';
                            break;
                        case 2:
                            $status = '已完成';
                            break;
                        case 3:
                            $status = '无效订单';
                            break;
                    }
                ?>
                <table class="mytable mycenter" width="80%">
                    <tr>
                        <td colspan="4">
                        <table width="100%">
                            <tr>
                                <td style="text-align: left;">
                                    <?php
                                        echo '订单号：' . $order['order_id'] . '<br>';
                                        echo '时间：' . $order['timestamp'] . '<br>';
                                        echo '状态：<span style="color: red;"><strong>' . $status . '</strong></span>';
                                    ?>
                                </td>
                                <td style="text-align: left;">
                                    <?php
                                        echo '收餐人姓名：' . $order['user_name'] . '<br>';
                                        echo '收餐人电话：' . $order['user_phone'] . '<br>';
                                        echo '收餐人地址：' . $order['user_address'];
                                    ?>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>

                    <tr style="background-color: #B2D460;">
                        <td>菜品名</td>
                        <td>单价(元)</td>
                        <td>数量</td>
                        <td>总价(元)</td>
                    </tr>
                <?php
                $content = json_decode($order['content'], true);
                $size = count($content);
                for($i=0; $i < $size;  $i++) {
                    $key = $content[$i];
                ?>

                        <tr>
                            <td>
                                <?php echo $key['name'];?>
                            </td>
                            <td>
                                <?php echo $key['price'];?>
                            </td>
                            <td>
                                <?php echo $key['count'];?>
                            </td>
                            <?php
                            if($i==0) {
                                echo '<td rowspan="' . $size . '"><strong>' . $order['price'] . '</strong></td>';
                            }
                            ?>
                        </tr>

                <?php } ?>

                </table>

                <br>
                <br>
                <?php if($order['status']==0) { ?>
                    <!-- 如果商家还没有处理订单，用户可以取消此订单 -->
                <div class="textcenter">
                    <a id="cancelbtn" onclick="cancelOrder(<?php echo $_GET['id'];?>)" href="#" class="btn medium">取消订单</a>
                </div>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
</body>
</html>