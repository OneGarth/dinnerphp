<?php require_once 'functions.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>修改密码</title>
    <script src="static/js/jquery-1.7.1.js"></script>
    <script src="static/js/jquery.validate.min.js"></script>
    <script src="static/js/jquery.gritter.min.js"></script>
    <script src="static/js/password.js"></script>
    <link href="static/css/basic.css" rel="stylesheet">
    <link href="static/css/common.css" rel="stylesheet">
    <link href="static/css/restaurant.css" rel="stylesheet">
    <link href="static/css/jquery-ui.min.css" rel="stylesheet">
    <link href="static/css/jquery.ui.theme.css" rel="stylesheet">
    <link href="static/css/account_login.css" rel="stylesheet">
    <link href="static/css/jquery.gritter.css" rel="stylesheet">
</head>
<body>
<?php include 'header.php'; ?>

<div class="page-wrap">
    <div class="inner-wrap">

        <div class="page-body block">
            <h2 class="title1 padding20 text-center">修改密码</h2>
            <div id="login-panal">
                    <form style="margin: 0 auto;text-align: center;" action="login.action.php" data-ajax="true" data-ajax-method="Post"
                          data-ajax-success="onSuccessed" id="passwordform" method="post" novalidate="novalidate">
                        <table class="form-table" style="margin: 0 auto;text-align: center;">
                            <tbody>
                            <tr>
                                <td style="width: 100px;" class="text-column">
                                    <label for="username">原密码</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required=""
                                           id="pwdOld" name="pwdOld" placeholder="" type="password" value="" autocomplete="off" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDiEFu6xIcAAAAXhJREFUOMvNk8FLVFEUxn/ffRdmIAla1CbBFDGCpoiQWYlBLty7UHAvEq2HYLhveDMws2/TIly6E9SdIEj+AVYgRaTgXhe2C968x2nhTOjow8pNZ/ede/ide893Lvx3UavVhkMIk30dQqiGECpF9e68CCG8LpfL3yStAAIk6Z2kT3Ect68C+AGdSroFVEII82aWSXoGYGYHVwE0qOM43pU0BXw3s1zSI2AnSZKXhYB6vT7inLvd7XZ/eu8fOOe2JEW9zjkwZ2bHkoayLDtpt9ufLzzBe/8GWC6VSpc7nIE2pLPLeu/fA0uDQ3T/6pp6039uZnfN7Ieke1EUrQOu3/VawPloNBrbwIyZ7TvnLvg/+mKOJ3xk88NR4R4sADM92fp9MDRMdXaRxenHVMbuFy8SMAFkZval2Wyu9ZN3Hk4zWx0nAtKsWwxotVrNNE2f5nn+CrB+/nRvlSR5y2EK0TWbSKfT+fo3Lribfr4bA/yfl56y2kkuZX8BjXVyqMs8oFcAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
                                    <span class="field-validation-valid" data-valmsg-for="UserName" data-valmsg-replace="true"></span>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px;" class="text-column">
                                    <label for="password">新密码</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required="请输入密码"
                                           id="Password" name="password" type="password" value="" autocomplete="off" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDiEFu6xIcAAAAXhJREFUOMvNk8FLVFEUxn/ffRdmIAla1CbBFDGCpoiQWYlBLty7UHAvEq2HYLhveDMws2/TIly6E9SdIEj+AVYgRaTgXhe2C968x2nhTOjow8pNZ/ede/ide893Lvx3UavVhkMIk30dQqiGECpF9e68CCG8LpfL3yStAAIk6Z2kT3Ect68C+AGdSroFVEII82aWSXoGYGYHVwE0qOM43pU0BXw3s1zSI2AnSZKXhYB6vT7inLvd7XZ/eu8fOOe2JEW9zjkwZ2bHkoayLDtpt9ufLzzBe/8GWC6VSpc7nIE2pLPLeu/fA0uDQ3T/6pp6039uZnfN7Ieke1EUrQOu3/VawPloNBrbwIyZ7TvnLvg/+mKOJ3xk88NR4R4sADM92fp9MDRMdXaRxenHVMbuFy8SMAFkZval2Wyu9ZN3Hk4zWx0nAtKsWwxotVrNNE2f5nn+CrB+/nRvlSR5y2EK0TWbSKfT+fo3Lribfr4bA/yfl56y2kkuZX8BjXVyqMs8oFcAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
                                    <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px;" class="text-column">
                                    <label for="password">再次输入新密码</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required="请输入密码"
                                           id="Password2" name="password2" type="password" value="" autocomplete="off" style="background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QsPDiEFu6xIcAAAAXhJREFUOMvNk8FLVFEUxn/ffRdmIAla1CbBFDGCpoiQWYlBLty7UHAvEq2HYLhveDMws2/TIly6E9SdIEj+AVYgRaTgXhe2C968x2nhTOjow8pNZ/ede/ide893Lvx3UavVhkMIk30dQqiGECpF9e68CCG8LpfL3yStAAIk6Z2kT3Ect68C+AGdSroFVEII82aWSXoGYGYHVwE0qOM43pU0BXw3s1zSI2AnSZKXhYB6vT7inLvd7XZ/eu8fOOe2JEW9zjkwZ2bHkoayLDtpt9ufLzzBe/8GWC6VSpc7nIE2pLPLeu/fA0uDQ3T/6pp6039uZnfN7Ieke1EUrQOu3/VawPloNBrbwIyZ7TvnLvg/+mKOJ3xk88NR4R4sADM92fp9MDRMdXaRxenHVMbuFy8SMAFkZval2Wyu9ZN3Hk4zWx0nAtKsWwxotVrNNE2f5nn+CrB+/nRvlSR5y2EK0TWbSKfT+fo3Lribfr4bA/yfl56y2kkuZX8BjXVyqMs8oFcAAAAASUVORK5CYII=); background-attachment: scroll; background-position: 100% 50%; background-repeat: no-repeat no-repeat;">
                                    <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-column"></td>
                            </tr>
                            <tr>
                                <td class="text-column"></td>
                                <td class="input-column">
                                    <button onclick="return editPassword();" value="提交"
                                            class="btn large" id="btn_login">提交</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>


            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
</body>
</html>