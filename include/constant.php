<?php
error_reporting (E_ALL);
/********* 常量 或者 在开发过程中不确定的值 *********/

define('SITE_NAME', 'DDinner');

define('DATABASE_NAME', 'dinner');
define('TABLE_ADMIN', 'admin');
define('TABLE_CATEGORY', 'category');
define('TABLE_DISH', 'dish');
define('TABLE_USER', 'user');
define('TABLE_ORDER', 'order');
define('TABLE_INFO', 'info');

// 地址
define('ADDRESS_PRE', '广东省湛江市赤坎区');

// 图片上传地址,注意前后有/号
define('URL_PIC_UPLOAD', '/mnt/shared/www/dinner/images/');
// 图片路径，注意前后有/号
define('URL_PIC', '/dinner/images/');
//首页
define('URL_INDEX', '/dinner/');
//登录网址
define('URL_LOGIN', 'login.php');
//中心网址
define('URL_CENTER', 'index.php');

/********* admin *********/
//首页
define('URL_ADMIN_INDEX', '/dinner/admin/');
//登录网址
define('URL_ADMIN_LOGIN', '/dinner/admin/login.php');
//中心网址
define('URL_ADMIN_CENTER', '/dinner/admin/index.php');
?>