<?php
require_once 'auth.php';
require_once 'functions.php';

if(!isset($_POST['action'])) {
    if(!isset($_GET['action'])) {
        echo 'error: no action method.';
        return;
    } else {
        $action = $_GET['action'];
    }
} else {
    $action = $_POST['action'];
}


// 永久删除该用户
if($action=='delete') {
    $row = userDelete($_POST['id']);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 编辑或新建
if($action=='edit') {
    $id = $_POST['_id'];

    if(!isEmail($_POST['email'])) {
        echo '请输入正确的邮箱地址';
        return;
    }

    if($id == '' && userHas($_POST['email'])) {
        echo '此Email已注册';
        return;
    }

    if($_POST['name'] == '') {
        echo '名称不能为空';
        return;
    }

    if($_POST['address'] == '') {
        echo '地址不能为空';
        return;
    }

    if($_POST['phone'] == '') {
        echo '电话不能为空';
        return;
    }

    $password = $_POST['pwd'];
    if($password == '') {
        if($id != '') {
            unset($_POST['pwd']);
        } else {
            echo '请输入密码';
            return;
        }
    } else {
        if($password != $_POST['pwd2']) {
            echo '两次输入的密码不一致';
            return;
        }else if(strlen($password) < 6 || strlen($password) > 16) {
            echo '密码长度只能在6到16之间';
            return;
        } else {
            $_POST['password'] = md5($password);
        }
    }

    unset($_POST['_id']);
    unset($_POST['pwd']);
    unset($_POST['pwd2']);
    if(trim($id)=='') {
            $_POST['register_time'] = date('Y-m-d H:i:s');
            $result = userInsert($_POST);
    } else {
        $result = userUpdate($id, $_POST);
    }

    if($result >= 0) {
        echo 'ok';
    } else {
        echo '提交失败，请稍候重试';
    }
    return;
}

?>