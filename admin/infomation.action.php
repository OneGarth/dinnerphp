<?php
require_once 'auth.php';
require_once 'functions.php';

if(isset($_SESSION['adminInfo']['_id'])
    && $_SESSION['adminInfo']['_id'] != ''
    && adminLevel($_SESSION['adminInfo']['_id']) > 3) {
    // 有权限
} else {
    echo '<script>alert("你没有权限");window.location.href="index.php";</script>';
    return;
}


// 编辑
    $data = array(
        'address'=>$_POST['address'],
        'phone'=>$_POST['phone'],
        'notice'=>$_POST['notice'],
        'time1'=>$_POST['h11'] . ':' . $_POST['m11'] . '--' . $_POST['h12'] . ':' . $_POST['m12'],
        'time2'=>$_POST['h21'] . ':' . $_POST['m21'] . '--' . $_POST['h22'] . ':' . $_POST['m22']
    );
    $result = infoUpdate($_POST['id'], $data);

    if($result > 0) {
        echo 'ok';
    } else {
        echo '提交失败，请稍候重试';
    }


?>