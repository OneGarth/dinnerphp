
$(document).ready(function(){


});

function newDialog() {
    document.getElementById('editTitle').innerText='新建';
    document.getElementById('editInputName').value = '';
    $('#editSubmitBtn').attr("onclick", "");
    $('#editSubmitBtn').bind("click", function () {
        if(document.getElementById('editInputName').value == '') {
            $('#myEmpty').modal('show');
        } else {
            doAction('new', '',document.getElementById('editInputName').value);
            $('#myEdit').modal('hide');
        }
    });
    $('#myEdit').modal('show');
}

function editDialog(id,name) {
    document.getElementById('editTitle').innerText='编辑';
    document.getElementById('editInputName').value = name;
    $('#editSubmitBtn').attr("onclick", "");
    $('#editSubmitBtn').bind("click", function () {
        if(document.getElementById('editInputName').value == '') {
            $('#myEmpty').modal('show');
        } else {
            doAction('edit', id, document.getElementById('editInputName').value);
            $('#myEdit').modal('hide');
        }
    });
    $('#myEdit').modal('show');
}

// 执行操作前显示确认对话框
function deleteAlert(id,name) {
    $('#deleteSubmitBtn').attr("onclick", "");
    $('#deleteSubmitBtn').bind("click", function () {
        doAction('delete', id,name);
    });
    $('#myAlert').modal('show');
}

// N秒后自动刷新当前页面
function redirect()
{
    var second = document.getElementById('totalSecond').innerText;
    if (second <= 0)
    {
//        window.location.reload();
    } else
    {
        second = second - 1;
        document.getElementById('totalSecond').innerText = second;
    }
}

// 各个操作
function doAction(action,id,name) {
    $.ajax({
        //this is the php file that processes the data and send mail
        url: "categories.action.php",
        //GET method is used
        type: "POST",
        //pass the data
        data: "action=" + action + "&id=" + id + "&name=" + name,
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html == 'ok') {
                $('#myModal').modal('show');
                setInterval("redirect()", 1000); //每1秒钟调用redirect()方法一次
                setTimeout(function(){window.location.reload();},3000);
            }  else {
                $.gritter.add({
                    title:	'提示',
                    text:	'提交失败，请稍候重试……',
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {

        }
    });
}