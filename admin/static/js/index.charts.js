
/**
 * 每日订单总价数据表
 */
$(document).ready(function(){
    initStatisticsData();
    orderPrice7day();
});

// 获取统计数据
function initStatisticsData(){
    $.ajax({
        dataType: 'json',
        //this is the php file that processes the data and send mail
        url: "index.action.php",
        //GET method is used
        type: "POST",
        //pass the data
        data: "action=init",
        //Do not cache the page
        cache: false,
        //success
        success: function(data) {
            $('#UserAll').html(data.UserAll);
            $('#UserNew').html(data.UserNew);
            $('#OrderCountAll').html(data.OrderCountAll);
            $('#OrderCountNew').html(data.OrderCountNew);
            $('#OrderPriceAll').html(data.OrderPriceAll);
            $('#OrderPriceNew').html(data.OrderPriceNew);
        },

        error: function() {
            $('#UserAll').html('加载失败');
            $('#UserNew').html('加载失败');
            $('#OrderCountAll').html('加载失败');
            $('#OrderCountNew').html('加载失败');
            $('#OrderPriceAll').html('加载失败');
            $('#OrderPriceNew').html('加载失败');
        },

        complete: function() {

        }
    });
}

// ajax加载统计图表
function orderPrice7day() {
        $.ajax({
            dataType: 'json',
            //this is the php file that processes the data and send mail
            url: "index.action.php",
            //GET method is used
            type: "POST",
            //pass the data
            data: "action=chart",
            //Do not cache the page
            cache: false,
            //success
            success: function(data) {
                loadChart(data);
            },

            error: function() {

            },

            complete: function() {

            }
        });
}


function loadChart(orderData) {
    var pv=[],t;
    //创建随机数据
    for(var i=0;i<7;i++){
        t = orderData[i];
        pv.push(t);
    }

    var data = [
        {
            name : '成交金额',
            value:pv,
            color:'#4FB9F0',
            line_width:2
        }
    ];

    //创建x轴标签文本
    var date = new Date();
    date.setDate(date.getDate()-8);
    var labels = [];
    for(var i=0;i<7;i++){
        date.setDate(date.getDate()+1);
        labels.push(date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate());
    }

    var chart = new iChart.LineBasic2D({
        render : 'canvasDiv',
        data: data,
        align:'center',
        title : '最近7天成交金额统计',
        subtitle : '',
        footnote : '',
        width : 740,
        height : 340,
        background_color:'#FEFEFE',
        tip:{
            enable:true,
            shadow:true,
            move_duration:400,
            border:{
                enable:true,
                radius : 5,
                width:2,
                color:'#3f8695'
            },
            listeners:{
                //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
                parseText:function(tip,name,value,text,i){
                    return name + ": " + value + "元";
                }
            }
        },
        tipMocker:function(tips,i){
            return "<div style='font-weight:600'>"+
                labels[Math.floor(i)]+" "+//日期
                "</div>"+tips.join("<br/>");
        },
        legend : {
            enable : true,
            row:1,//设置在一行上显示，与column配合使用
            column : 'max',
            valign:'top',
            sign:'bar',
            background_color:null,//设置透明背景
            offsetx:-80,//设置x轴偏移，满足位置需要
            border : true
        },
        crosshair:{
            enable:true,
            line_color:'#f68f70'//十字线的颜色
        },
        sub_option : {
            label:false,
            point_size:10
        },
        coordinate:{
            width:640,
            height:240,
            axis:{
                color:'#dcdcdc',
                width:1
            },
            scale:[{
                position:'left',
                start_scale:0,
                end_scale:100,
                scale_space:10,
                scale_size:2,
                scale_color:'#9f9f9f'
            },{
                position:'bottom',
                labels:labels
            }]
        }
    });

    //开始画图
    chart.draw();
}