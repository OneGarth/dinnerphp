// 提交表单
function submitForm() {
    $('#editForm').submit();
}


// N秒后自动刷新当前页面
function redirect()
{
    var second = document.getElementById('totalSecond').innerText;
    if (second <= 0)
    {

    } else
    {
        second = second - 1;
        document.getElementById('totalSecond').innerText = second;
    }
}



//表单验证
function validForm() {
    var editForm = $('#editForm');
    var submitBtn = $('#submitBtn');

    // Form Validation
    $("#editForm").validate({
        rules:{
            name:{
                required:true
            },
            email:{
              required: true,
              email: true
            },
            pwd:{
              minlength:6,
              maxlength:16
            },
            pwd2: {
              minlength:6,
              maxlength:16,
              equalTo:"#pwd"
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        },
        invalidHandler: function(event, validator) {
            $.gritter.add({
                title:	'提示',
                text:	'请填写完整信息',
                time: 3000,
                sticky: false
            });
            $('#submitBtn').button('reset');
        },
        submitHandler: function(form) {
            $('#submitBtn').button('loading');
            $.ajax({
                    //this is the php file that processes the data and send mail
                    url: "admins.action.php?action=edit",
                    //GET method is used
                    type: "POST",
                    //pass the data
                    data: $('#editForm').serialize(),
                    //Do not cache the page
                    cache: false,
                    //success
                    success: function(html) {
                        if(html == 'ok') {
                            $('#myModal').modal('show');
                            setInterval("redirect()", 1000); //每1秒钟调用redirect()方法一次
                            setTimeout(function(){window.location.href='admins.php';},3000);
                        } else {
                            $.gritter.add({
                                title:	'提示',
                                text:	html,
                                time: 3000,
                                sticky: false
                            });
                        }
                    },

                    error: function() {
                        $.gritter.add({
                            title:	'提示',
                            text:	'系统有误，请联系系统管理员',
                            time: 3000,
                            sticky: false
                        });
                    },

                    complete: function() {
                        submitBtn.button('reset');
                    }
                });
            }
    });
}