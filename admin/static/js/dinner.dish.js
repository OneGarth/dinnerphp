// 获取菜品数据
$(document).ready(function(){

    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "bProcessing": true,
        "sAjaxSource": 'dishes.data.php?cat=' + $('#cat').val()
    });

    $('input[type=checkbox],input[type=radio],input[type=file]').uniform();

    $('select').select2();

    $("span.icon input:checkbox, th input:checkbox").click(function() {
        var checkedStatus = this.checked;
        var checkbox = $(this).parents('.widget-box').find('tr td:first-child input:checkbox');
        checkbox.each(function() {
            this.checked = checkedStatus;
            if (checkedStatus == this.checked) {
                $(this).closest('.checker > span').removeClass('checked');
            }
            if (this.checked) {
                $(this).closest('.checker > span').addClass('checked');
            }
        });
    });
});


// 删除确认对话框
function deleteAlert(id, name) {
    $('#deleteSubmitBtn').attr("onclick", "");
    $('#deleteSubmitBtn').bind("click", function () {
        doAction('delete', id)
    });
    $('#deleteContent').text('确定要删除 ' + name + ' ？');
    $('#myAlert').modal('show');
}

// N秒后自动刷新当前页面
function redirect()
{
    var second = document.getElementById('totalSecond').innerText;
    if (second <= 0)
    {
//        window.location.reload();
    } else
    {
        second = second - 1;
        document.getElementById('totalSecond').innerText = second;
    }
}

// 各个操作
function doAction(action,id) {
    $.ajax({
        //this is the php file that processes the data and send mail
        url: "dishes.action.php",
        //GET method is used
        type: "POST",
        //pass the data
        data: "action=" + action + "&id=" + id,
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html == 'ok') {
                $('#myModal').modal('show');
                setInterval("redirect()", 1000); //每1秒钟调用redirect()方法一次
                setTimeout(function(){window.location.reload();},3000);
            }  else {
                $.gritter.add({
                    title:	'提示',
                    text:	'提交失败，请稍候重试……',
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {

        }
    });
}