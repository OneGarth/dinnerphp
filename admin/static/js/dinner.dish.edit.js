// 提交表单
function submitForm() {
    $('#editForm').submit();
    $('#submitBtn').button('loading');
}


// N秒后自动刷新当前页面
function redirect()
{
    var second = $('#totalSecond').innerText;
    if (second <= 0)
    {
//        window.location.href='dishes.php';
    } else
    {
        second = second - 1;
        $('#totalSecond').innerText = second;
    }
}



//表单验证
function validForm() {
    var editForm = $('#editForm');
    var submitBtn = $('#submitBtn');

    // Form Validation
    $("#editForm").validate({
        rules:{
            name:{
                required:true
            },
            count:{
                required:true,
                number: true,
                min: 0
            },
            price:{
                required: true,
                number: true,
                min: 1
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        },
        invalidHandler: function(event, validator) {
            $.gritter.add({
                title:	'提示',
                text:	'请填写完整信息',
                time: 3000,
                sticky: false
            });
            $('#submitBtn').button('reset');
        },
        submitHandler: function(form) {
            if(document.getElementById('image').value=='') {
                $.gritter.add({
                    title:	'提示',
                    text:	'请上传图片',
                    time: 3000,
                    sticky: false
                });
                $('#submitBtn').button('reset');
            } else {
                $.ajax({
                    //this is the php file that processes the data and send mail
                    url: "dishes.action.php?action=edit",
                    //GET method is used
                    type: "POST",
                    //pass the data
                    data: $('#editForm').serialize(),
                    //Do not cache the page
                    cache: false,
                    //success
                    success: function(html) {
                        if(html == 'ok') {
                            $('#myModal').modal('show');
                            setInterval("redirect()", 1000); //每1秒钟调用redirect()方法一次
                            setTimeout(function(){window.location.href='dishes.php';},3000);
                        } else {
                            $.gritter.add({
                                title:	'提示',
                                text:	html,
                                time: 3000,
                                sticky: false
                            });
                        }
                    },

                    error: function() {
                        $.gritter.add({
                            title:	'提示',
                            text:	'系统有误，请联系系统管理员',
                            time: 3000,
                            sticky: false
                        });
                    },

                    complete: function() {
                        submitBtn.button('reset');
                    }
                });
            }
        }
    });
}

// 图片上传
function uploadImage() {
    var progress = $('#uploadProgressBar');
    var bar = $('#uploadPercent');
    var showimg = $('#dishImage');
    var btn = $(".uploader span");
//    $("#fileupload").wrap("<form id='myupload' action='dishes.action.php?action=uploadImage' method='post' enctype='multipart/form-data'></form>");
    $("#fileupload").change(function () { //选择文件
        if($('#fileupload').val() != '') {
            $("#myupload").ajaxSubmit({
                dataType: 'json',
                beforeSend: function () { //开始上传
                    progress.show(); //显示进度条
                    var percentVal = '0%'; //开始进度为0%
                    bar.width(percentVal); //进度条的宽度
                    bar.html(percentVal); //显示进度为0%
                    btn.html("上传中..."); //上传按钮显示上传中
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%'; //获得进度
                    bar.width(percentVal); //上传进度条宽度变宽
                    bar.html(percentVal); //显示上传进度百分比
                },
                success: function (data) {
                    progress.fadeOut('fast');
                    if(data.result=='ok') {//上传成功
                        //显示上传后的图片
                        showimg.html("<img width='250px' src='" + data.image + "'>");
                        document.getElementById('image').value= data.message;
                    } else {//上传失败
                        $.gritter.add({
                            title:	data.result,
                            text:	data.message,
                            time: 3000,
                            sticky: false
                        });
                    }
                    btn.html("上传图片"); //上传按钮还原
                },
                error: function (xhr) { //上传失败
                    btn.html("上传失败");
                    bar.width('0');
                    files.html(xhr.responseText); //返回失败信息
                }
            });
        }
    });
}