
$(document).ready(function(){

	var login = $('#loginform');
	var recover = $('#recoverform');
    var loginbtn = $('#loginbtn');
	var speed = 400;

    // Form Validation
    $("#loginform").validate({
        rules:{
            required:{
                required:true
            },
            email:{
                required:true,
                email: true
            },
            password:{
                required: true,
                minlength:6,
                maxlength:20
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        },
        invalidHandler: function(event, validator) {
            $.gritter.add({
                title:	'提示',
                text:	'请填写完整信息',
                time: 3000,
                sticky: false
            });
        },
        submitHandler: function(form) {
            $('#loginbtn').button('loading');
            $.ajax({
                //this is the php file that processes the data and send mail
                url: "login.action.php",
                //GET method is used
                type: "POST",
                //pass the data
                data: $('#loginform').serialize(),
                //Do not cache the page
                cache: false,
                //success
                success: function(html) {
                    if(html == 'ok') {
                        location.href="index.php";
                        $.gritter.add({
                            title:	'提示',
                            text:	'登录成功，跳转中……',
                            time: 5000,
                            sticky: false
                        });
                    } else {
                        document.getElementById('captcha_img').src = '../include/captcha.php?'+Math.random();
                        $.gritter.add({
                            title:	'提示',
                            text:	html,
                            time: 3000,
                            sticky: false
                        });
                    }
                },

                error: function() {
                    document.getElementById('captcha_img').src = '../include/captcha.php?'+Math.random();
                    $.gritter.add({
                        title:	'提示',
                        text:	'系统有误，请联系系统管理员',
                        time: 3000,
                        sticky: false
                    });
                },

                complete: function() {
                    $('#loginbtn').button('reset');
                }
            });
        }
    });


    // Form Validation
    $("#recoverform").validate({
        rules:{
            required:{
                required:true
            },
            recoveremail:{
                required:true,
                email: true
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight:function(element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });

	$('#to-recover').click(function(){
		
		$("#loginform").slideUp();
		$("#recoverform").fadeIn();
	});
	$('#to-login').click(function(){
		
		$("#recoverform").hide();
		$("#loginform").fadeIn();
	});
	
	
	$('#to-login').click(function(){
	
	});
    
    if($.browser.msie == true && $.browser.version.slice(0,3) < 10) {
        $('input[placeholder]').each(function(){ 
       
        var input = $(this);       
       
        $(input).val(input.attr('placeholder'));
               
        $(input).focus(function(){
             if (input.val() == input.attr('placeholder')) {
                 input.val('');
             }
        });
       
        $(input).blur(function(){
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        });
    });

        
        
    }
});