<?php
require_once 'auth.php';
require_once 'functions.php';

if(isset($_GET['cat']) && $_GET['cat'] != -2) {
    if($_GET['cat'] == -1) {
        $result = dishGetRecommend();
    } else {
        $result = dishGetByCategory($_GET['cat']);
    }
} else {
    $result = dishGetAll();
}


$out_content = '';
$aaData = array();


for($i = 0; $i < count($result); $i++) {
    $dish = $result[$i];
    $button = '<a class="btn btn-mini btn-info" href="dishes.edit.php?id='.$dish['_id'].'">编辑</a>'
        .'&nbsp;&nbsp;&nbsp;'
        .'<buttn class="btn btn-mini btn-danger" onclick="deleteAlert(\''.$dish['_id'].'\',\''.$dish['name'].'\')"
                            >删除</buttn>'
        .'</div>';
    $data = array();
    if($dish['recommend'] == '1') {
        $data[0] = $dish['name'].' &nbsp;&nbsp;<span class="badge badge-important">荐</span>';
    } else {
        $data[0] = $dish['name'];
    }
    $data[1] = $dish['category_name'];
    $data[2] = '￥'.$dish['price'];
    $data[3] = $button;
    $aaData[$i] = $data;
}

echo json_encode(array("aaData"=>$aaData));

?>