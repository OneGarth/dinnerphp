<?php
require_once 'auth.php';
require_once 'functions.php';

if(!isset($_POST['action'])) {
    if(!isset($_GET['action'])) {
        echo 'error: no action method.';
        return;
    } else {
        $action = $_GET['action'];
    }
} else {
    $action = $_POST['action'];
}



// 设置所有菜品数量
if($action=='updateAllCount') {
    $row = dishUpdateAllCount($_POST['id']);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}


// 永久删除该菜品
if($action=='delete') {
    $row = dishDelete($_POST['id']);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 菜品编辑或新建
if($action=='edit') {
    if(!is_numeric($_POST['price']) || $_POST['price'] <= 0) {
        echo '价格只能是大于0的数字';
        return;
    }

    $id = $_POST['dishId'];
    $imageOld = trim($_POST['imageOld']);
    unset($_POST['imageOld']);
    unset($_POST['dishId']);
    if(trim($id)=='') {
        $result = dishInsert($_POST);
    } else {
        $result = dishUpdate($id, $_POST);
    }

    if($result > 0) {
        // 更新成功后删除旧图片
        if($imageOld != '' && trim($_POST['image']) != $imageOld) {
            // 删除旧图片
//            $name = substr($imageOld, strripos($imageOld,'/'));
            unlink(URL_PIC_UPLOAD .$imageOld);
        }
        echo 'ok';
    } else {
        echo '提交失败，请稍候重试';
    }
    return;
}


 //上传菜品图片
if($action=='uploadImage') {
    $picname = $_FILES['mypic']['name'];
    $picsize = $_FILES['mypic']['size'];
    $result = 'error';
    $message = '上传错误';
    $path = '';
    if ($picname != "") {
        if ($picsize > 512000) { //限制上传大小
            $result = 'error';
            $message = '图片大小不能超过500k';
        }
        $type = strstr($picname, '.'); //限制上传格式
        if ($type != ".gif" && $type != ".jpg" && $type != ".jpeg" && $type != ".png" && $type != ".gif" && $type != ".bmp") {
            $result = 'error';
            $message = '图片格式不对！';
        } else {
            $upload_dir = URL_PIC_UPLOAD;
            if (!is_dir($upload_dir))
                mkdir($upload_dir, 0777);
            $rand = rand(100, 999);
            $pics = date("YmdHis") . $rand . $type; //命名图片名称
            //上传路径
            $pic_path = $upload_dir. $pics;
            if (!move_uploaded_file($_FILES['mypic']['tmp_name'], $pic_path)) { //执行上传
                $result = 'error';
                $message = '上传失败，未知错误!'; //上传失败，错误未知
            } else {
                $result = 'ok';
                $message = $pics;
                $path = URL_PIC . $pics;
            }
        }
    } else {
        $result = 'error';
        $message = '没有选择图片文件';
    }
    $arr = array(
        'result'=>$result,
        'message'=>$message,
        'image'=>$path
    );
    echo json_encode($arr); //输出json数据
}
?>