<?php
require_once 'auth.php';
require_once 'functions.php';

$mainType='index.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>DDinner</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="static/css/matrix-style.css" />
    <link rel="stylesheet" href="static/css/matrix-media.css" />
    <link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="static/css/jquery.gritter.css" />
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body>

<?php include('header.php'); ?>
<?php include('sidebar.php'); ?>

<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
  <div class="container-fluid">
    <div class="quick-actions_homepage">
      <ul class="quick-actions">
        <li class="bg_lg span4"> <a href="infomation.php"> <i class="icon-signal"></i> 店铺信息</a> </li>
        <li class="bg_lr"> <a href="categories.php"> <i class="icon-inbox"></i> 分类管理 </a> </li>
        <li class="bg_lb"> <a href="orders.php"> <i class="icon-th"></i> 菜品管理</a> </li>
        <li class="bg_ls"> <a href="users.php"> <i class="icon-user"></i> 用户管理</a> </li>
        <li class="bg_ly"> <a href="admins.php"> <i class="icon-user"></i> 管理员管理</a> </li>
        <li class="bg_lo span4"> <a href="orders.php"> <i class="icon-th-list"></i> 订单管理</a> </li>
        <li class="bg_lr"> <a href="categories.php"> <i class="icon-tint"></i> 添加分类</a> </li>
        <li class="bg_lb"> <a href="dishes.edit.php"> <i class="icon-pencil"></i>添加菜品</a> </li>
        <li class="bg_ls"> <a href="users.edit.php"> <i class="icon-plus"></i> 添加用户</a> </li>
        <li class="bg_ly"> <a href="admins.edit.php"> <i class="icon-plus"></i> 添加管理员</a> </li>

      </ul>
    </div>
<!--End-Action boxes-->    

<!--Chart-box-->    
    <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
          <h5>数据分析</h5>
        </div>
        <div class="widget-content" >
          <div class="row-fluid">
            <div class="span9">
<!--              <div class="chart"></div>-->
                <div id='canvasDiv'></div>
            </div>
            <div class="span3">
              <ul class="site-stats">
                <li class="bg_lh"><i class="icon-user"></i> <strong id="UserAll">加载中...</strong> <small>用户总数</small></li>
                <li class="bg_lh"><i class="icon-plus"></i> <strong id="UserNew">加载中...</strong> <small>昨日新增用户</small></li>
                  <li class="bg_lh"><i class="icon-tag"></i> <strong id="OrderCountAll">加载中...</strong> <small>订单总数</small></li>
                  <li class="bg_lh"><i class="icon-globe"></i> <strong id="OrderCountNew">加载中...</strong> <small>昨日订单总数</small></li>
                <li class="bg_lh"><i class="icon-shopping-cart"></i> <strong id="OrderPriceAll">加载中...</strong> <small>总成交金额</small></li>
                <li class="bg_lh"><i class="icon-repeat"></i> <strong id="OrderPriceNew">加载中...</strong> <small>昨日成交金额</small></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
<!--End-Chart-box-->
  </div>
</div>

<!--end-main-container-part-->

<?php include('footer.php'); ?>


<script src="static/js/jquery.min.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/matrix.js"></script>
<script src="static/js/jquery.gritter.min.js"></script>

<script src="static/js/index.charts.js"></script>
<script src="static/js/ichart.1.2.min.js"></script>

<script>

</script>

<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
</body>
</html>
