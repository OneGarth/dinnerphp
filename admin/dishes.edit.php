<?php
require_once 'auth.php';
require_once 'functions.php';

$mainType='dishes.php';

if(isset($_GET['id'])) {
    $title = '修改菜品';
    $dish = dishGetById($_GET['id']);
} else {
    $title = '新建菜品';
    $dish =  array();
    $dish['_id'] = '';
    $dish['name'] = '';
    $dish['category_id'] = '';
    $dish['detail'] = '';
    $dish['image'] = '/images/default.jpg';
    $dish['price'] = '';
    $dish['count'] = '0';
    $dish['status'] = '1';
    $dish['recommend'] = '0';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>菜品管理</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="static/css/uniform.css" />
    <link rel="stylesheet" href="static/css/select2.css" />
    <link rel="stylesheet" href="static/css/matrix-style.css" />
    <link rel="stylesheet" href="static/css/matrix-media.css" />
    <link rel="stylesheet" href="static/css/jquery.gritter.css" />
    <link rel="stylesheet" href="static/css/colorpicker.css" />
    <link rel="stylesheet" href="static/css/datepicker.css" />
    <link rel="stylesheet" href="static/css/bootstrap-wysihtml5.css" />
    <link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body>

<?php
include('header.php');
include('sidebar.php');
?>


<div id="content">
  <div id="content-header">
    <div id="breadcrumb">
        <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="dishes.php" >菜品管理</a>
        <a href="#" class="current"><?php echo $title;?></a>
    </div>
  </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5><?php echo $title; ?></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form id="editForm" action="" method="post" class="form-horizontal">

                            <div class="control-group">
                                <label class="control-label">名称</label>
                                <div class="controls">
                                    <input type="text" id="name" name="name" class="span4" value="<?php echo $dish['name'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">分类</label>
                                <div class="controls">
                                    <select class="span4" id="category_id" name="category_id">
                                        <?php
                                        $categories = categoryGetAll();
                                        foreach($categories as $cat):
                                        ?>
                                           <option <?php echo $cat['_id']==$dish['category_id']? 'selected':'';?>
                                               value="<?php echo $cat['_id'];?>"><?php echo $cat['name'];?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">单价</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input class="span4" id="price" name="price" value="<?php echo $dish['price'];?>" type="text" placeholder="单价" >
                                        <span class="add-on">元</span> </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">状态</label>
                                <div class="controls">
                                    <label>
                                        <input type="radio"
                                            <?php echo $dish['status']==1? 'checked="checked"':'';?>
                                               name="status" value="1"/>
                                        上架</label>
                                    <label>
                                        <input type="radio"
                                               <?php echo $dish['status']==0? 'checked="checked"':'';?>
                                               name="status" value="0"/>
                                        下架</label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">推荐</label>
                                <div class="controls">
                                    <label>
                                        <input type="radio"
                                            <?php echo $dish['recommend']==1? 'checked="checked"':'';?>
                                               name="recommend" value="1"/>
                                        推荐</label>
                                    <label>
                                        <input type="radio"
                                            <?php echo $dish['recommend']==0? 'checked="checked"':'';?>
                                               name="recommend" value="0"/>
                                        默认</label>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">描述</label>
                                <div class="controls">
                                    <textarea name="detail" class="span11"><?php echo trim($dish['detail']);?></textarea>
                                </div>
                            </div>

                            <input name="dishId" type="hidden" value="<?php echo $dish['_id'];?>">
                            <input name="imageOld" type="hidden" value="<?php echo $dish['image'];?>">
                            <input id="image" name="image" type="hidden" value="<?php echo $dish['image'];?>">

                        </form>

                        <form id="myupload" class="form-horizontal" action="dishes.action.php?action=uploadImage"
                              method="post" enctype="multipart/form-data">
                            <div class="control-group">
                                    <label class="control-label">图片</label>
                                    <div class="controls">
                                        <input type="file" id="fileupload" name="mypic" />
                                        <div id="uploadProgressBar"
                                             style="width: 30%;height:20px;margin-top: 10px;margin-bottom: 0px;"
                                             class="hide progress progress-mini active progress-striped">
                                            <div id="uploadPercent" style="width: 70%;" class="bar">70%></div>
                                        </div>
                                        <div id="dishImage" style="margin-top:10px;" >
                                            <?php
                                            if(!empty($dish['image'])) {
                                                echo '<img width="250px" src="'.URL_PIC.$dish['image'].'"/>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <div class="form-horizontal">
                                    <div class="controls">
                                        <button onclick="submitForm()" id="submitBtn" class="btn btn-success"
                                                data-loading-text="提交中...">提交</button>
                                        &nbsp;&nbsp;
                                        <a href="dishes.php" id="submitBtn" class="btn"
                                           data-loading-text="取消中...">取消</a>
                                    </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


        <!-- dialog start -->
        <div class="widget-content">

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>提示</h3>
                </div>
                <div class="modal-body alert-success alert-block">
                    <p>操作成功， <span id="totalSecond" class="badge badge-info">2</span> 秒后自动跳转……</p>
                </div>
            </div>

        </div>
        <!-- dialog end -->
</div>

<!--Footer-part-->
<?php include('footer.php'); ?>

<!--end-Footer-part-->
<script src="static/js/jquery.min.js"></script>
<script src="static/js/jquery.ui.custom.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/jquery.uniform.js"></script>
<script src="static/js/jquery.form.js"></script>
<script src="static/js/select2.min.js"></script>
<script src="static/js/jquery.gritter.min.js"></script>
<script src="static/js/matrix.js"></script>
<script src="static/js/masked.js"></script>
<script src="static/js/matrix.form_common.js"></script>
<script src="static/js/jquery.validate.js"></script>
<script src="static/js/dinner.dish.edit.js"></script>

<script>
    uploadImage();
    validForm();
</script>
</body>
</html>
