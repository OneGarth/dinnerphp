<?php
require_once 'auth.php';
require_once 'functions.php';

if(!isset($_POST['action'])) {
    if(!isset($_GET['action'])) {
        echo 'error: no action method.';
        return;
    } else {
        $action = $_GET['action'];
    }
} else {
    $action = $_POST['action'];
}

// 加载统计数据
if($action == 'init') {
    // NEW为昨天的
    $today = date('Y-m-d');
    $yesterday = date('Y-m-d', strtotime('-1 days'));

    $UserAll = userCountAll();
    $UserNew = userCount($yesterday, $today);

    $OrderCountAll = orderAllCount();
    $OrderCountNew = orderCount($yesterday, $today);
    $OrderPriceAll = orderSumAll();
    $OrderPriceNew = orderSum($yesterday, $today);

    $arr = array(
        'UserAll'=>$UserAll,
        'UserNew'=>$UserNew,
        'OrderCountAll'=>$OrderCountAll,
        'OrderCountNew'=>$OrderCountNew,
        'OrderPriceAll'=>$OrderPriceAll,
        'OrderPriceNew'=>$OrderPriceNew
    );
    echo json_encode($arr); //输出json数据
    return;
}

// 图表
if($action == 'chart') {
    $arr = array();
    for($i = -7; $i < 0; $i++) {
        $from = date('Y-m-d', strtotime($i.' days'));
        $to = date('Y-m-d', strtotime(($i+1).' days'));
        $price = orderSum($from, $to);
        $arr[$i+7] = $price;
    }
    echo json_encode($arr); //输出json数据
    return;
}

?>