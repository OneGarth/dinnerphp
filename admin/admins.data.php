<?php
require_once 'auth.php';
require_once 'functions.php';

if(isset($_SESSION['adminInfo']['_id'])
    && $_SESSION['adminInfo']['_id'] != ''
    && adminLevel($_SESSION['adminInfo']['_id']) > 5) {
    // 有权限
} else {
    echo '<script>alert("你没有权限");window.location.href="index.php";</script>';
    return;
}

$result = adminGetAll();

$aaData = array();


for($i = 0; $i < count($result); $i++) {
    $admin = $result[$i];
    $button = '<a class="btn btn-mini btn-info" href="admins.edit.php?id='.$admin['_id'].'">编辑</a>'
        .'&nbsp;&nbsp;&nbsp;'
        .'<buttn class="btn btn-mini btn-danger" onclick="deleteAlert(\''.$admin['_id'].'\',\''.$admin['email'].'\')"
                            >删除</buttn>'
        .'</div>';
    $data = array();
    $data[0] = $admin['email'];
    $data[1] = $admin['name'];
    $data[2] = $admin['level'];
    $data[3] = $button;
    $aaData[$i] = $data;
}

echo json_encode(array("aaData"=>$aaData));

?>