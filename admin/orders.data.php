<?php
require_once 'auth.php';
require_once 'functions.php';

if(isset($_GET['type'])) {
    $type = $_GET['type'];
} else {
    $type = 0;
}

switch($type) {
    case 0:
        $result = orderGetUndeal();
        break;
    case 1:
        $result = orderGetDealing();
        break;
    case 2:
        $result = orderGetFinished();
        break;
    case 3:
        $result = orderGetInvalid();
        break;
    case 4:
        $result = orderGetAll();
        break;
    default:
        $result = orderGetUndeal();
}

$out_content = '';
$aaData = array();




for($i = 0; $i < count($result); $i += 1) {
   $order = $result[$i];

    $content = json_decode(stripslashes($order['content']), true);
    $out_content = '';
    foreach((array)$content as $c) {
        $out_content.= $c['name'].'(￥'.$c['price'].') x '.$c['count'].'<br />';
    }
    $length = count($out_content) - count('<br />');
    $out_content = $out_content.substr($out_content, 0, $length);

    $button = '无';
    switch($order['status']) {
        case 0:
            $status = '<span class="label label-info">待处理</span>';
            $button = '<buttn class="btn btn-mini btn-info" onclick="doAction(\'dealing\',\''.$order['order_id'].'\')"
                            data-loading-text="提交中...">已配送</buttn>'
                        .'&nbsp;&nbsp;&nbsp;'
                        .'<buttn class="btn btn-mini btn-danger" onclick="doActionBeforeAlert(\'invalid\',\''.$order['order_id'].'\')"
                            data-loading-text="提交中...">无效</buttn>'
                        .'</div>';
            break;
        case 1:
            $status = '<span class="label label-warning">配送中</span>';
            $button = '<buttn class="btn btn-mini btn-success" onclick="doAction(\'finished\',\''.$order['order_id'].'\')"
                        data-loading-text="提交中...">已完成</buttn>';
            break;
        case 2:
            $status = '<span class="label label-success">已完成</span>';
            $button = '<buttn class="btn btn-mini btn-danger" onclick="doActionBeforeAlert(\'delete\',\''.$order['order_id'].'\')"
                            data-loading-text="提交中...">永久删除</buttn>';
            break;
        case 3:
            $status = '<span class="label label-important">无效</span>';
            $button = '<buttn class="btn btn-mini btn-danger" onclick="doActionBeforeAlert(\'delete\',\''.$order['order_id'].'\')"
                            data-loading-text="提交中...">永久删除</buttn>';
    }

    $data = array();
    $data[0] = $order['order_id'].'<br />'.$status;
    $data[1] = $order['timestamp'];
    $data[2] = $order['user_id'].'<br>'.$order['user_name'].'('.$order['user_phone'].')'.'<br>'.$order['user_address'];
    $data[3] = $out_content;
    $data[4] = '￥'.$order['price'];
    $data[5] = $button;
    $aaData[$i] = $data;
}

echo json_encode(array("aaData"=>$aaData));


//{
//    "aaData": [
//    [
//        "<input type='checkbox' />",
//        "订单号",
//        "用户",
//        "时间",
//        "内容",
//        "操作"
//    ],
//    [
//        "<input type='checkbox' />",
//        "Trident",
//        "Internet Explorer 5.0",
//        "Win 95+",
//        "5",
//        "C"
//    ]
//    ]
//}
?>