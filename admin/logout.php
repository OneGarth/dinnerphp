<?php
session_start();
$_SESSION['admin'] = false;
$_SESSION['adminInfo'] = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>正在跳转……</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body style="
background-color: #2E363F;
padding: 0;
margin-top: 10%;">

<div style="width:30%;margin: 0 auto;">
<div class="alert alert-info alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">提示</h4>
    <br />
    已退出，<span id="totalSecond">3</span>秒后自动跳转到登录页面……
</div>
</div>

</body>
<script language="javascript" type="text/javascript">
    <!--
    var second = document.getElementById('totalSecond').innerText;

    setInterval("redirect()", 1000); //每1秒钟调用redirect()方法一次
    setTimeout(function(){window.location.href='login.php';},3000);
    function redirect()
    {
        if (second < 0)
        {
//            location.href = 'login.php';
        } else
        {
            document.getElementById('totalSecond').innerText = second--;
        }
    }
    -->
</script>
</html>