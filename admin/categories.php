<?php
require_once 'auth.php';
require_once 'functions.php';

$mainType='categories.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>分类管理</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="static/css/bootstrap.min.css" />
<link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="static/css/matrix-style.css" />
<link rel="stylesheet" href="static/css/matrix-media.css" />
<link rel="stylesheet" href="static/css/jquery.gritter.css" />
<link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body>

<?php
include('header.php');
include('sidebar.php');
?>

<!--main-container-part-->
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="#" class="current">分类管理</a> </div>
    <h1>分类管理</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title">
            <span class="icon"><i class="icon-file"></i></span>
            <h5>分类列表</h5>
              <span onclick="newDialog()" data-original-title="新建" class="tip pull-right" style="cursor:pointer;padding: 9px 10px 7px 11px;border-left: 1px solid #dadada;">
                <i class="icon-plus-sign"></i>
              </span>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>分类ID</th>
                  <th>分类名称</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>

              <?php
              $data = categoryGetAll();
              foreach($data as $category):
              ?>
                <tr>
                  <td class="taskStatus"><?php echo $category['_id'];?></td>
                  <td class="taskStatus"><?php echo $category['name'];?></td>
                  <td class="taskOptions">
                      <a href="#" class="tip-top" data-original-title="编辑"
                         onclick="editDialog('<?php echo $category['_id'];?>','<?php echo $category['name'];?>')">
                          <i class="icon-pencil"></i></a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="#" class="tip-top" data-original-title="删除"
                         onclick="deleteAlert('<?php echo $category['_id'];?>','<?php echo $category['name'];?>')">
                          <i class="icon-remove"></i></a></td>
                </tr>
              <?php
              endforeach;
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>


    <!-- dialog start -->
    <div class="widget-content">

        <div id="myEdit" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3 id="editTitle">提示</h3>
            </div>
            <div class="modal-body">
                <p>
                    <input type="text" id="editInputName" placeholder="请输入分类名称">
                </p>
            </div>
            <div class="modal-footer">
                <button id="editSubmitBtn" class="btn btn-primary">确定</button>
                <a data-dismiss="modal" class="btn" href="#">取消</a>
            </div>
        </div>

        <div id="myModal" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3>提示</h3>
            </div>
            <div class="modal-body alert-success alert-block">
                <p>操作成功， <span id="totalSecond" class="badge badge-info">2</span> 秒后自动跳转……</p>
            </div>
        </div>

        <div id="myAlert" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3>警告</h3>
            </div>
            <div class="modal-body alert-danger alert-block">
                <p>确定要执行此操作？</p>
            </div>
            <div class="modal-footer">
                <button id="deleteSubmitBtn" data-dismiss="modal" class="btn btn-primary">确定</button>
                <a data-dismiss="modal" class="btn" href="#">取消</a>
            </div>
        </div>

        <div id="myEmpty" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button">×</button>
                <h3>警告</h3>
            </div>
            <div class="modal-body alert-danger alert-block">
                <p>分类名称不能为空</p>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-primary">确定</button>
            </div>
        </div>
    </div>
    <!-- dialog end -->


</div>
<!--main-container-part-->

<!--Footer-part-->
<?php include('footer.php'); ?>
<!--end-Footer-part-->

<script src="static/js/jquery.min.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/jquery.ui.custom.js"></script>
<script src="static/js/matrix.js"></script>
<script src="static/js/jquery.gritter.min.js"></script>
<script src="static/js/dinner.category.js"></script>
</body>
</html>
