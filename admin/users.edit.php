<?php
require_once 'auth.php';
require_once 'functions.php';

$mainType='users.php';

if(isset($_GET['id'])) {
    $title = '修改用户';
    $user = userGetById($_GET['id']);
} else {
    $title = '新建用户';
    $user =  array();
    $user['_id'] = '';
    $user['name'] = '';
    $user['email'] = '';
    $user['phone'] = '';
    $user['address'] = '';
    $user['credit'] = '0';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>用户管理</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="static/css/uniform.css" />
    <link rel="stylesheet" href="static/css/select2.css" />
    <link rel="stylesheet" href="static/css/matrix-style.css" />
    <link rel="stylesheet" href="static/css/matrix-media.css" />
    <link rel="stylesheet" href="static/css/jquery.gritter.css" />
    <link rel="stylesheet" href="static/css/colorpicker.css" />
    <link rel="stylesheet" href="static/css/datepicker.css" />
    <link rel="stylesheet" href="static/css/bootstrap-wysihtml5.css" />
    <link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body>

<?php
include('header.php');
include('sidebar.php');
?>


<div id="content">
  <div id="content-header">
    <div id="breadcrumb">
        <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="dishes.php" >用户管理</a>
        <a href="#" class="current"><?php echo $title;?></a>
    </div>
  </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5><?php echo $title; ?></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form id="editForm" action="" method="post" class="form-horizontal">

                            <div class="control-group">
                                <label class="control-label">名称</label>
                                <div class="controls">
                                    <input type="text" id="name" name="name" class="span4"
                                           value="<?php echo $user['name'];?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input id="email" name="email" value="<?php echo $user['email'];?>" class="span4">
                                </div>
                            </div>

                            <?php if($user['_id']!='') {
                                $placeholder = '如果不修改密码，请置空';
                            } else {
                                $placeholder = '请输入密码';
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">密码</label>
                                <div class="controls">
                                    <input class="span4" id="pwd" name="pwd"
                                           type="password" placeholder="<?php echo $placeholder;?>" >
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">再次输入密码</label>
                                <div class="controls">
                                    <input class="span4" id="pwd2" name="pwd2"
                                           type="password" placeholder="<?php echo $placeholder;?>" >
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">联系电话</label>
                                <div class="controls">
                                    <input class="span4" id="phone" name="phone"
                                           type="text" value="<?php echo $user['phone'];?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">地址</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <span class="add-on"><?php echo ADDRESS_PRE;?></span>
                                        <input class="span4" id="address" name="address"
                                               value="<?php echo $user['address'];?>" type="text" >
                                         </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">积分</label>
                                <div class="controls">
                                    <div class="input-append">
                                        <input class="span4" id="credit" name="credit"
                                               value="<?php echo $user['credit'];?>" type="text" >
                                    </div>
                                </div>
                            </div>

                            <input name="_id" type="hidden" value="<?php echo $user['_id'];?>">

                        </form>

                            <hr>
                            <div class="form-horizontal">
                                    <div class="controls">
                                        <button onclick="submitForm()" id="submitBtn" class="btn btn-success"
                                                data-loading-text="提交中...">提交</button>
                                        &nbsp;&nbsp;
                                        <a href="users.php" id="submitBtn" class="btn"
                                           data-loading-text="取消中...">取消</a>
                                    </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


        <!-- dialog start -->
        <div class="widget-content">

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>提示</h3>
                </div>
                <div class="modal-body alert-success alert-block">
                    <p>操作成功， <span id="totalSecond" class="badge badge-info">2</span> 秒后自动跳转……</p>
                </div>
            </div>

        </div>
        <!-- dialog end -->
</div>

<!--Footer-part-->
<?php include('footer.php'); ?>

<!--end-Footer-part-->
<script src="static/js/jquery.min.js"></script>
<script src="static/js/jquery.ui.custom.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/jquery.uniform.js"></script>
<script src="static/js/jquery.form.js"></script>
<script src="static/js/select2.min.js"></script>
<script src="static/js/jquery.gritter.min.js"></script>
<script src="static/js/matrix.js"></script>
<script src="static/js/masked.js"></script>
<script src="static/js/jquery.validate.js"></script>
<script src="static/js/dinner.user.edit.js"></script>

<script>
    validForm();
</script>
</body>
</html>
