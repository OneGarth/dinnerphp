<?php

?>



<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> 管理面板</a>
    <ul>
        <li <?php echo ($mainType=='index.php')? 'class="active"': '';?>>
            <a href="index.php"><i class="icon icon-home"></i> <span>管理面板</span></a>
        </li>
        <li <?php echo ($mainType=='infomation.php')? 'class="active"': '';?>>
            <a href="infomation.php"><i class="icon icon-signal"></i> <span>店铺信息</span></a>
        </li>
        <li <?php echo ($mainType=='orders.php')? 'class="active"': '';?>>
            <a href="orders.php"><i class="icon icon-th"></i> <span>订单管理</span>
                <?php
                $orderUndealCount = orderGetUndealCount();
                if($orderUndealCount > 0) {
                    echo('<span class="label label-important">'.$orderUndealCount.'</span>');
                }
                ?>
            </a>
        </li>
        <li <?php echo ($mainType=='categories.php')? 'class="active"': '';?>>
            <a href="categories.php"><i class="icon icon-inbox"></i> <span>分类管理</span></a>
        </li>
        <li <?php echo ($mainType=='dishes.php')? 'class="active"': '';?>>
            <a href="dishes.php"><i class="icon icon-fullscreen"></i> <span>菜品管理</span></a>
        </li>
        <li <?php echo ($mainType=='users.php')? 'class="active"': '';?>>
            <a href="users.php"><i class="icon icon-tint"></i> <span>用户管理</span></a>
        </li>
        <li <?php echo ($mainType=='admins.php')? 'class="active"': '';?>
            ><a href="admins.php"><i class="icon icon-pencil"></i> <span>管理员管理</span></a>
        </li>

        <!-- 底部进度条 -->
        <?php
        $undeal = orderGetUndealCount();
        $dealing = orderGetDealingCount();
        $sum = $undeal + $dealing;
        $undealPercent =  round($dealing / $sum, 3) * 100;
        $dealingPercent =  round($undeal / $sum, 3) * 100;
        ?>
        <li class="content"> <span>待处理任务进度</span>
            <div class="progress progress-mini progress-danger active progress-striped">
                <div style="width: <?php echo $undealPercent;?>%;" class="bar"></div>
            </div>
            <span class="percent"><?php echo $undealPercent;?>%</span>
            <div class="stat">剩余数量：<?php echo $undeal;?></div>
        </li>
        <li class="content"> <span>配送中任务进度</span>
            <div class="progress progress-mini active progress-striped">
                <div style="width: <?php echo $dealingPercent;?>%;" class="bar"></div>
            </div>
            <span class="percent"><?php echo $dealingPercent;?>%</span>
            <div class="stat">剩余数量：<?php echo $dealing;?></div>
        </li>
    </ul>
</div>
<!--sidebar-menu-->




<?php

?>