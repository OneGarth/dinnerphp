<?php
require_once 'auth.php';
require_once 'functions.php';

// 设置为配送中
if(isset($_POST['action']) && $_POST['action']=='new') {
    $row = categoryInsert($_POST['name']);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 设置为已完成
if(isset($_POST['action']) && $_POST['action']=='edit') {
    $row = categoryUpdate($_POST['id'], $_POST['name']);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 设置为无效订单
if(isset($_POST['action']) && $_POST['action']=='delete') {
    $row = categoryDelete($_POST['id']);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

?>