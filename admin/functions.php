<?php
require_once '../include/db.php';

/**
 * 更新店铺信息
 */
function infoUpdate($id, $data) {
    global $db;
    $r = $db->update(TABLE_INFO, $data, array('_id'=>$id));
    if($r <= 0) {
        return $db->insert(TABLE_INFO, $data);
    } else {
        return $r;
    }
}

/**
 * 获取店铺信息
 * @return array|bool|null|string
 */
function infoGet() {
    global $db;
    $r = $db->select(TABLE_INFO, '*');
    if($r != null && count($r) > 0) {
        return $r[0];
    } else {
        return null;
    }
}

/**************统计 START ************/

function dishCountAll() {
    global $db;
    return $db->count(TABLE_DISH);
}

/**
 * 统计2个日期之间新增的用户数量
 * @param $from
 * @param $to
 * @return int|string
 */
function userCount($from, $to) {
    global $db;
    if($from > $to) {
        $temp = $from;
        $from = $to;
        $to = $temp;
    }
    return $db->count(TABLE_USER, array('register_time[<>]'=>array($from, $to)));
}

/**
 * 统计用户总数
 * @return int|string
 */
function userCountAll() {
    global $db;
    return $db->count(TABLE_USER);
}

/**
 * 统计2个日期之间非无效订单的总价
 * @param $from
 * @param $to
 * @return int|string
 */
function orderSum($from, $to) {
    global $db;
    if($from > $to) {
        $temp = $from;
        $from = $to;
        $to = $temp;
    }
    return $db->sum(TABLE_ORDER, 'price',
        array('AND'=>array('timestamp[<>]'=>array($from,$to),array('status[!]'=>3))));
}

/**
 * 所有非无效订单的总价
 * @return int|string
 */
function orderSumAll() {
    global $db;
    return $db->sum(TABLE_ORDER, 'price', array('status[!]'=>3));
}

/**
 * 统计2个日期之间的非无效订单数量
 * @param $from
 * @param $to
 * @return int|string
 */
function orderCount($from, $to) {
    global $db;
    if($from > $to) {
        $temp = $from;
        $from = $to;
        $to = $temp;
    }
    return $db->count(TABLE_ORDER, array('AND'=>array('timestamp[<>]'=>array($from,$to), 'status[!]'=>3)));
}

/**
 * 时间比较函数，返回两个日期相差几秒、几分钟、几小时或几天
 * */
function DateDiff($date1, $date2, $unit = "") {
    switch ($unit) {
        case 's':
            $dividend = 1;
            break;
        case 'i':
            $dividend = 60; //oSPHP.COM.CN
            break;
        case 'h':
            $dividend = 3600;
            break;
        case 'd':
            $dividend = 86400;
            break; //开源OSPhP.COM.CN
        default:
            $dividend = 86400;
    }
    $time1 = strtotime($date1);
    $time2 = strtotime($date2);
    if ($time1 && $time2)
        return (float)($time1 - $time2) / $dividend;
    return false;
}


/******** 管理员 START *************/

/**
 * 管理员等级
 * @param $id
 * @return mixed
 */
function adminLevel($id) {
    global $db;
    $result = $db->select(TABLE_ADMIN, '*', array('_id'=>$id));
    return $result[0]['level'];
}

function adminHas($email) {
    global $db;
    $result = $db->select(TABLE_ADMIN, '*', array('email'=>$email));
    if(count($result) > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * 删除管理员
 * @param $id
 * @return int
 */
function adminDelete($id) {
    global $db;
    return $db->delete(TABLE_ADMIN, array('_id'=>$id));
}


/**
 * 更新管理员
 */
function adminUpdate($id, $data) {
    global $db;
    return $db->update(TABLE_ADMIN, $data, array('_id'=>$id));
}

/**
 * 新建管理员
 */
function adminInsert($data) {
    global $db;
    return $db->insert(TABLE_ADMIN, $data);
}

/**
 * 获取某个管理员
 * @param $id
 * @return mixed
 */
function adminGetById($id) {
    global $db;
    $result = $db->select(TABLE_ADMIN, '*', array('_id'=>$id));
    return $result[0];
}

function adminGetAll() {
    global $db;
    return $db->select(TABLE_ADMIN, '*');
}

/******** 管理员 END *************/




/******** 用户 START *************/

function userLogin($email, $password) {
    global $db;
    $result = $db->select(TABLE_USER, '*', array('AND'=>array('email'=>$email, 'password'=>md5($password))));
    if(count($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function userHas($email) {
    global $db;
    $result = $db->select(TABLE_USER, '*', array('email'=>$email));
    if(count($result) > 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * 删除用户
 * @param $id
 * @return int
 */
function userDelete($id) {
    global $db;
    return $db->delete(TABLE_USER, array('_id'=>$id));
}


/**
 * 更新用户
 */
function userUpdate($id, $data) {
    global $db;
    return $db->update(TABLE_USER, $data, array('_id'=>$id));
}

/**
 * 新建用户
 */
function userInsert($data) {
    global $db;
    return $db->insert(TABLE_USER, $data);
}

/**
 * 获取某个用户
 * @param $id
 * @return mixed
 */
function userGetById($id) {
    global $db;
    $result = $db->select(TABLE_USER, '*', array('_id'=>$id));
    return $result[0];
}

function userGetAll() {
    global $db;
    return $db->select(TABLE_USER, '*');
}

/******** 用户 END *************/


/******** 菜品 START *************/

/**
 * 删除菜品
 * @param $id
 * @return int
 */
function dishDelete($id) {
    global $db;
    return $db->delete(TABLE_DISH, array('_id'=>$id));
}

/**
 * 更新菜品
 */
function dishUpdate($id, $data) {
    global $db;
    return $db->update(TABLE_DISH, $data, array('_id'=>$id));
}

/**
 * 新建菜品
 */
function dishInsert($data) {
    global $db;
    return $db->insert(TABLE_DISH, $data);
}

/**
 * 获取某个菜品
 * @param $id
 * @return mixed
 */
function dishGetById($id) {
    global $db;
    $result = $db->select(TABLE_DISH, '*', array('_id'=>$id));
    return $result[0];
}

/**
 * 获取某分类下的所有菜品
 * @param $catId
 * @return array|bool|string
 */
function dishGetRecommend(){
    global $db;
    return $db->select(TABLE_DISH,
        array('[><]'.TABLE_CATEGORY=>array('category_id'=>'_id')),
        array(TABLE_DISH.'._id', TABLE_DISH.'.name', TABLE_DISH.'.detail', TABLE_DISH.'.image',
            TABLE_DISH.'.price', TABLE_DISH.'.status', TABLE_DISH.'.recommend',
            TABLE_DISH.'.category_id', TABLE_CATEGORY.'.name(category_name)'),
        array(TABLE_DISH.'.recommend'=>1, 'ORDER'=>TABLE_DISH.'._id DESC'));
}

/**
 * 获取某分类下的所有菜品
 * @param $catId
 * @return array|bool|string
 */
function dishGetByCategory($catId){
    global $db;
    return $db->select(TABLE_DISH,
        array('[><]'.TABLE_CATEGORY=>array('category_id'=>'_id')),
        array(TABLE_DISH.'._id', TABLE_DISH.'.name', TABLE_DISH.'.detail', TABLE_DISH.'.image',
            TABLE_DISH.'.price', TABLE_DISH.'.status', TABLE_DISH.'.recommend',
            TABLE_DISH.'.category_id', TABLE_CATEGORY.'.name(category_name)'),
        array(TABLE_DISH.'.category_id'=>$catId, 'ORDER'=>TABLE_DISH.'._id DESC'));
}

/**
 * 获取所有菜品
 * @return array|bool|string
 */
function dishGetAll() {
    global $db;
    return $db->select(TABLE_DISH,
        array('[><]'.TABLE_CATEGORY=>array('category_id'=>'_id')),
        array(TABLE_DISH.'._id', TABLE_DISH.'.name', TABLE_DISH.'.detail', TABLE_DISH.'.image',
            TABLE_DISH.'.price', TABLE_DISH.'.status', TABLE_DISH.'.recommend',
            TABLE_DISH.'.category_id', TABLE_CATEGORY.'.name(category_name)'),
        array('ORDER'=>TABLE_DISH.'._id DESC'));
}

/******** 菜品 END *************/



/******** 分类 START *************/

function categoryDelete($catId) {
    global $db;
    return $db->delete(TABLE_CATEGORY, array('_id'=>$catId));
}

/**
 * 更新分类名称
 * @param $catId
 * @param $name
 * @return int
 */
function categoryUpdate($catId, $name) {
    global $db;
    return $db->update(TABLE_CATEGORY, array('name'=>$name), array('_id'=>$catId));
}

/**
 * 新建分类
 * @param $name
 * @return array|string
 */
function categoryInsert($name) {
    global $db;
    return $db->insert(TABLE_CATEGORY, array('name'=>$name));
}

/**
 * 获取所有分类
 * @return array|bool|string
 */
function categoryGetAll() {
    global $db;
    return $db->select(TABLE_CATEGORY, '*');
}

/******** 分类 END *************/



/******** 订单 START *************/

/**
 * 订单设置为完成后，调用此方法添加用户积分
 * @param $orderId
 */
function updateUserCredit($orderId) {
    global $db;
    $order = orderGetById($orderId);
    $user = userGetById($order['user_id']);
    $db->update(TABLE_USER, array('credit'=>$user['credit']+$order['price']), array('_id'=>$user['_id']));
}

/**
 * 获取所有订单数量
 * @return int|string
 */
function orderAllCount() {
    global $db;
    return $db->count(TABLE_ORDER);
}

/**
 * 设置为无效订单
 * @param $orderId
 * @return int
 */
function orderSetInvalid($orderId) {
    global $db;
    return $db->update(TABLE_ORDER, array("status"=>3), array("order_id"=>$orderId));
}

/**
 * 设置为已完成订单
 * @param $orderId
 * @return int
 */
function orderSetFinished($orderId) {
    global $db;
    return $db->update(TABLE_ORDER, array("status"=>2), array("order_id"=>$orderId));
}

/**
 * 设置为配送中订单
 * @param $orderId
 * @return int
 */
function orderSetDealing($orderId) {
    global $db;
    return $db->update(TABLE_ORDER, array("status"=>1), array("order_id"=>$orderId));
}

/**
 * 永久删除订单
 * @param $orderId
 * @return int
 */
function orderSetDeleted($orderId) {
    global $db;
    return $db->delete(TABLE_ORDER, array("order_id"=>$orderId));
}
/**
 * 获取待处理订单数量
 * @return int|string
 */
function orderGetUndealCount() {
    global $db;
    return $db->count(TABLE_ORDER, array("status"=>0));
}

/**
 * 获取配送中订单数量
 * @return int|string
 */
function orderGetDealingCount() {
    global $db;
    return $db->count(TABLE_ORDER, array("status"=>1));
}

/**
 * 根据订单ID获取订单
 * @param $id
 * @return null
 */
function orderGetById($id) {
    global $db;
    $result = $db->select(TABLE_ORDER, '*', array('order_id'=>$id));
    if($result != null && count($result) > 0) {
        return $result[0];
    } else {
        return null;
    }
}

/**
 * 所有未处理订单
 */
function orderGetUndeal() {
    global $db;
    return $db->select(TABLE_ORDER, '*', array('status'=>'0', 'ORDER'=>'timestamp DESC'));
}

/**
 * 所有配送中的订单
 */
function orderGetDealing() {
    global $db;
    return $db->select(TABLE_ORDER, '*', array('status'=>'1', 'ORDER'=>'timestamp DESC'));
}

/**
 * 所有已完成订单
 */
function orderGetFinished() {
    global $db;
    return $db->select(TABLE_ORDER, '*', array('status'=>'2', 'ORDER'=>'timestamp DESC'));
}

/**
 * 所有历史订单
 */
function orderGetInvalid() {
    global $db;
    return $db->select(TABLE_ORDER, '*', array('status'=>'3', 'ORDER'=>'timestamp DESC'));
}

/**
 * 所有订单
 * @return array|bool|string
 */
function orderGetAll() {
    global $db;
    return $db->select(TABLE_ORDER, '*', array('ORDER'=>'timestamp DESC'));
}

/******** 订单 END *************/

/**
 * 判断字符串是不是手机号码
 * @param $phone
 * @return bool
 */
function isPhone($phone) {
    if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/",$phone)) {
        return true;
    } else {
        return false;
    }
}

/**
 * 判断是否为邮箱地址
 * @param $email
 * @return bool
 */
function isEmail($email) {
    $preg=preg_match("/\\w+([-+.']\\w+)*@\\w+\\.\\w+([-.]\\w+)*/",trim($email));
    if($preg) {
        return true;
    } else {
        return false;
    }
}

/**
 * 字符串不为空返回true
 * @param $value
 * @return bool
 */
function isInvalid($value) {
    if($value != null && $value != '') {
        return false;
    } else {
        return true;
    }
}

/**
 * 管理员登录
 * @param $email
 * @param $password
 * @return array
 */
function adminLogin($email, $password) {
    if(!isEmail($email)) {
        $result = 'error';
        $message = '邮箱地址格式不正确';
    } elseif(isInvalid($password)) {
        $result = 'error';
        $message = '密码不能为空';
    } else {
        global $db;
        $row = $db->select(TABLE_ADMIN, "*",
            array("AND"=>
                array("email"=>$email, "password"=>md5($password)))
        );
        if(count($row) > 0) {
            $result = 'ok';
            $message = '登录成功';
            setLogin($row[0]);
        } else {
            $result = 'error';
            $message = '邮箱地址或密码不正确';
            setLogout();
        }
    }
    return array("result"=>$result, "message"=>$message);
}

/**
 * 检查是否已经登录
 * @param $goLogin
 * @return bool
 */
function checkAdminLogin($goLogin) {
	 //防止全局变量造成安全隐患
	$admin = false;
	 // 启动会话，这步必不可少
	if(!isset($_SESSION)){
	    session_start();
	} 
	if(isset($_SESSION["admin"]) && $_SESSION["admin"] === true){
		return true;
	} else {
		setLogout();
		if($goLogin) {
			goLogin();
		}
		return false;
	}
}

/**
 * 登录成功后设置SESSION
 * @param $user
 */
function setLogin($user) {
	if(!isset($_SESSION)){
	    session_start();
	} 
	$_SESSION['admin'] = true;
	$_SESSION['adminInfo'] = $user;
}

/**
 * 登出
 */
function setLogout() {
	unset($_SESSION['admin']);
	unset($_SESSION['adminInfo']);
}

/**
 * 登出并跳转到登录页
 */
function adminLogout() {
	unset($_SESSION['admin']);
	unset($_SESSION['adminInfo']);
	goLogin();
	exit();
}

function goCenter() {
	echo '<script language="JavaScript">location.href="index.php";</script>';
}

function goLogin() {
	echo '<script language="JavaScript">location.href="login.php";</script>';
}

function showAlert($msg) {
	echo '<script language="JavaScript">alert("'.$msg.'");</script>';
}

function goBack() {
    echo '<script language="javascript">history.go(-1);</script>';
}


?>