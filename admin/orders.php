<?php
require_once 'auth.php';
require_once 'functions.php';

$mainType='orders.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>订单</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="static/css/uniform.css" />
    <link rel="stylesheet" href="static/css/select2.css" />
    <link rel="stylesheet" href="static/css/matrix-style.css" />
    <link rel="stylesheet" href="static/css/matrix-media.css" />
    <link rel="stylesheet" href="static/css/jquery.gritter.css" />
    <link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body>


<?php
include('header.php');
include('sidebar.php');
?>


<div id="content">
  <div id="content-header">
    <div id="breadcrumb">
        <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="#" class="current">订单</a>
    </div>
  </div>
  <div class="container-fluid">

    <div class="row-fluid">
      <div class="span12">


          <?php
          if(isset($_GET['type'])) {
              $type = $_GET['type'];
          } else {
              $type = 0;
          }
          ?>
          <input type="hidden" id="order_type" name="order_type" value="<?php echo($type);?>" />

          <div class="widget-box">
              <div class="widget-title">
                  <ul class="nav nav-tabs">
                      <li <?php echo ($type==0? 'class="active"' : '');?>><a  href="?type=0">待处理订单</a></li>
                      <li <?php echo ($type==1? 'class="active"' : '');?>><a  href="?type=1">配送中订单</a></li>
                      <li <?php echo ($type==2? 'class="active"' : '');?>><a  href="?type=2">已完成订单</a></li>
                      <li <?php echo ($type==3? 'class="active"' : '');?>><a  href="?type=3">无效订单</a></li>
                      <li <?php echo ($type==4? 'class="active"' : '');?>><a  href="?type=4">所有订单</a></li>
                  </ul>
              </div>
              <div class="widget-content nopadding">
                  <div id="tab1" class="tab-pane active">

            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>订单号</th>
                  <th>时间</th>
                  <th>用户</th>
                  <th>订单内容</th>
                  <th>总价</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
                      </div>
          </div>
        </div>
      </div>


        <!-- dialog start -->
        <div class="widget-content">
            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>提示</h3>
                </div>
                <div class="modal-body alert-success alert-block">
                    <p>操作成功， <span id="totalSecond" class="badge badge-info">2</span> 秒后自动跳转……</p>
                </div>
            </div>
            <div id="myAlert" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>警告</h3>
                </div>
                <div class="modal-body alert-danger alert-block">
                    <p>确定要执行此操作？</p>
                </div>
                <div class="modal-footer">
                    <button id="deleteSubmitBtn" data-dismiss="modal" class="btn btn-primary">确定</button>
                    <a data-dismiss="modal" class="btn" href="#">取消</a>
                </div>
            </div>
        </div>
        <!-- dialog end -->
    </div>
  </div>
</div>

<!--Footer-part-->
<?php include('footer.php'); ?>

<!--end-Footer-part-->
<script src="static/js/jquery.min.js"></script>
<script src="static/js/jquery.ui.custom.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/jquery.uniform.js"></script>
<script src="static/js/select2.min.js"></script>
<script src="static/js/jquery.dataTables.min.js"></script>
<script src="static/js/jquery.gritter.min.js"></script>
<script src="static/js/matrix.js"></script>
<script src="static/js/dinner.order.js"></script>

</body>
</html>
