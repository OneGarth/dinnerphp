<?php
//防止全局变量造成安全隐患
$admin = false;
// 启动会话，这步必不可少
session_start();
// 判断是否登陆
if (isset($_SESSION["admin"]) && $_SESSION["admin"] === true) {
    echo '<script language="JavaScript">location.href="index.php";</script>';
} else {
    //  验证失败，将 $_SESSION["admin"] 置为 false
    $_SESSION["admin"] = false;
    $_SESSION["adminInfo"] = null;
}

?>

<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>登录</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="static/css/bootstrap.min.css" />
		<link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="static/css/matrix-login.css" />
        <link rel="stylesheet" href="static/css/jquery.gritter.css" />
        <link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->

    </head>
    <body>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" action="" method="post">
				 <div class="control-group normal_text"> <h3><img src="static/img/logo.png" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"></i></span>
                            <input type="text" placeholder="邮箱地址" name="email" id="email"/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>
                            <input type="password" placeholder="密码"  name="password" id="password"/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-exchange"></i></span>
                            <input style="width:43%;" type="text" placeholder="不区分大小写"
                                   name="captcha" id="captcha"/>
                            <span><img id="captcha_img"  title="点击刷新" src="../include/captcha.php"
                                 onclick="this.src='../include/captcha.php?'+Math.random();"></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions" style="text-align: center;">
<!--                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">忘记密码?</a></span>-->
                    <span>
                        <button type="submit" id="loginbtn" class="btn btn-success" data-loading-text="登录中..."> 登录</button>
                    </span>
                </div>
            </form>

<!--            -->
<!--            <form id="recoverform" action="#" class="form-vertical">-->
<!--				<p class="normal_text">请输入你的邮箱地址，我们将发送一封重置密码的邮件给你</p>-->
<!--                <div class="control-group">-->
<!--                    <div class="controls">-->
<!--                        <div class="main_input_box">-->
<!--                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span>-->
<!--                            <input type="text" placeholder="E-mail address" id="recoveremail" name="recoveremail"/>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--               -->
<!--                <div class="form-actions">-->
<!--                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; 返回</a></span>-->
<!--                    <span class="pull-right"><a class="btn btn-info"/>提交</a></span>-->
<!--                </div>-->
<!--            </form>-->
        </div>

        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>
        <script src="static/js/jquery.gritter.min.js"></script>
        <script src="static/js/jquery.validate.js"></script>
        <script src="static/js/matrix.login.js"></script>
    </body>

</html>