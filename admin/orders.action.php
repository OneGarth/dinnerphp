<?php
require_once 'auth.php';
require_once 'functions.php';

// 设置为配送中
if(isset($_POST['action']) && $_POST['action']=='dealing') {
    $orderId = $_POST['orderId'];
    $row = orderSetDealing($orderId);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 设置为已完成
if(isset($_POST['action']) && $_POST['action']=='finished') {
    $orderId = $_POST['orderId'];
    $row = orderSetFinished($orderId);
    if($row > 0) {
        // 增加用户积分
        updateUserCredit($orderId);
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 设置为无效订单
if(isset($_POST['action']) && $_POST['action']=='invalid') {
    $orderId = $_POST['orderId'];
    $row = orderSetInvalid($orderId);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}

// 永久删除该订单
if(isset($_POST['action']) && $_POST['action']=='delete') {
    $orderId = $_POST['orderId'];
    $row = orderSetDeleted($orderId);
    if($row > 0) {
        echo 'ok';
        return;
    } else {
        echo 'error';
        return;
    }
}
?>