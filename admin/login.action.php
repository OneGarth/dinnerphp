<?php
require_once 'functions.php';

session_start();
if(strtolower(trim($_POST['captcha'])) != $_SESSION['captcha']) {
    echo '验证码输入错误';
    return;
}

unset($_POST['captcha']);
$result = adminLogin($_POST['email'], $_POST['password']);
if($result["result"] == 'ok') {
    echo 'ok';
} else {
    echo $result["message"];
}

?>