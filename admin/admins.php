<?php
require_once 'auth.php';
require_once 'functions.php';

if(isset($_SESSION['adminInfo']['_id'])
    && $_SESSION['adminInfo']['_id'] != ''
    && adminLevel($_SESSION['adminInfo']['_id']) > 5) {
        // 有权限
} else {
    echo '<script>alert("你没有权限");window.location.href="index.php";</script>';
    return;
}

$mainType='admins.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>管理员管理</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="static/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="static/css/uniform.css" />
    <link rel="stylesheet" href="static/css/select2.css" />
    <link rel="stylesheet" href="static/css/matrix-style.css" />
    <link rel="stylesheet" href="static/css/matrix-media.css" />
    <link rel="stylesheet" href="static/css/jquery.gritter.css" />
    <link href="static/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>-->
</head>
<body>

<style>
    .table-bordered th, .table-bordered td {
        text-align: center;
    }
</style>

<?php
include('header.php');
include('sidebar.php');
?>


<div id="content">
  <div id="content-header">
    <div id="breadcrumb">
        <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
        <a href="#" class="current">管理员管理</a>
    </div>
  </div>
  <div class="container-fluid">

    <div class="row-fluid">
      <div class="span12">

          <div class="widget-box">
              <div class="widget-title">
                  <h5>管理员列表</h5>
                   <a href="admins.edit.php" data-original-title="新建" class="tip pull-right" style="cursor:pointer;padding: 9px 10px 7px 11px;border-left: 1px solid #dadada;">
                        <i class="icon-plus-sign"></i>&nbsp;&nbsp;新建管理员
                   </a>
              </div>
              <div class="widget-content nopadding">
                  <div id="tab1" class="tab-pane active">

            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>名称</th>
                  <th>等级</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
                      </div>
          </div>
        </div>
      </div>


        <!-- dialog start -->
        <div class="widget-content">

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>提示</h3>
                </div>
                <div class="modal-body alert-success alert-block">
                    <p>操作成功， <span id="totalSecond" class="badge badge-info">2</span> 秒后自动跳转……</p>
                </div>
            </div>
            <div id="myAlert" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button">×</button>
                    <h3>警告</h3>
                </div>
                <div class="modal-body alert-danger alert-block">
                    <p id="deleteContent">确定要执行此操作？</p>
                </div>
                <div class="modal-footer">
                    <button id="deleteSubmitBtn" data-dismiss="modal" class="btn btn-primary">确定</button>
                    <a data-dismiss="modal" class="btn" href="#">取消</a>
                </div>
            </div>

        </div>
        <!-- dialog end -->
    </div>
  </div>
</div>

<!--Footer-part-->
<?php include('footer.php'); ?>

<!--end-Footer-part-->
<script src="static/js/jquery.min.js"></script>
<script src="static/js/jquery.ui.custom.js"></script>
<script src="static/js/bootstrap.min.js"></script>
<script src="static/js/jquery.uniform.js"></script>
<script src="static/js/select2.min.js"></script>
<script src="static/js/jquery.dataTables.min.js"></script>
<script src="static/js/jquery.gritter.min.js"></script>
<script src="static/js/matrix.js"></script>
<script src="static/js/dinner.admin.js"></script>

</body>
</html>
