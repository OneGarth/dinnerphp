<?php require_once 'functions.php'; ?>
<?php
if(isLogin()) {
    go('index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>登录</title>
    <script src="static/js/jquery-1.7.1.js"></script>
    <script src="static/js/jquery.validate.min.js"></script>
    <script src="static/js/jquery.gritter.min.js"></script>
    <script src="static/js/login.js"></script>
    <link href="static/css/basic.css" rel="stylesheet">
    <link href="static/css/common.css" rel="stylesheet">
    <link href="static/css/restaurant.css" rel="stylesheet">
    <link href="static/css/jquery-ui.min.css" rel="stylesheet">
    <link href="static/css/jquery.ui.theme.css" rel="stylesheet">
    <link href="static/css/account_login.css" rel="stylesheet">
    <link href="static/css/jquery.gritter.css" rel="stylesheet">
</head>
<body>
<?php include 'header.php'; ?>

<div class="page-wrap">
    <div class="inner-wrap">

        <div class="page-body block">
            <h2 class="title1 padding20 text-center">登录或注册</h2>
            <div id="login-panal">
                <div id="login-panal-left">
                    <form action="login.action.php" data-ajax="true" data-ajax-method="Post"
                          data-ajax-success="onSuccessed" id="loginform" method="post" novalidate="novalidate">                        <table class="form-table">
                            <tbody>
                            <tr>
                                <td style="width: 40px;" class="text-column">
                                    <label for="username">邮箱</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required="请输入邮箱"
                                           id="UserName" name="email" placeholder="" type="text" value="" autocomplete="off">
                                    <span class="field-validation-valid" data-valmsg-for="UserName" data-valmsg-replace="true"></span>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px;" class="text-column">
                                    <label for="password">密码</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required="请输入密码"
                                           id="Password" name="password" type="password" value="" autocomplete="off">
                                    <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40px;" class="text-column">
                                    <label for="password">验证码</label>
                                </td>
                                <td class="input-column">
                                    <input style="width:90px;" class="input-text" data-val="true"
                                           id="captcha" name="captcha" type="text" value=""  >
                                    <span class="field-validation-valid">
                                        <img id="captcha_img"  title="点击刷新" src="include/captcha.php" align="absbottom"
                                              onclick="this.src='include/captcha.php?'+Math.random();">
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-column"></td>
                            </tr>
                            <tr>
                                <td class="text-column"></td>
                                <td class="input-column">
                                    <button onclick="return login();" value="登录"
                                            class="btn large" id="btn_login">登录</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>

                </div>
                <div id="login-panal-right">
                    <form action="login.action.php" data-ajax="true" data-ajax-method="Post"
                          data-ajax-success="onSuccessed" id="registerform" method="post" novalidate="novalidate">                        <table class="form-table">
                            <tbody>
                            <tr>
                                <td style="width: 48px;" class="text-column">
                                    <label for="username">邮箱</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required="请输入邮箱"
                                           id="UserName" name="email" placeholder="" type="text" value="">
                                    <span class="field-validation-valid" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 48px;" class="text-column">
                                    <label for="username">用户名</label>
                                </td>
                                <td class="input-column">
                                    <input id="UserName" name="name" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 48px;" class="text-column">
                                    <label for="password">创建密码</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-required="请输入密码"
                                           id="Password" name="pwd" type="password" value="" >
                                    <span class="field-validation-valid" data-valmsg-for="Password" data-valmsg-replace="true"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 48px;" class="text-column">
                                    <label for="password">重复密码</label>
                                </td>
                                <td class="input-column">
                                    <input class="input-text" data-val="true" data-val-equalto="密码和确认密码不匹配！"
                                           data-val-equalto-other="*.Password"
                                           data-val-length="密码长度范围在6-20个字符间"
                                           data-val-length-max="20" data-val-length-min="6"
                                           id="ConfirmPassword" name="pwd2" type="password" value="" />
                                    <span class="field-validation-valid" data-valmsg-for="ConfirmPassword" data-valmsg-replace="true"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 48px;" class="text-column">
                                    <label for="username">联系电话</label>
                                </td>
                                <td class="input-column">
                                    <input name="phone" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 48px;" class="text-column">
                                    <label for="username">常用地址</label>
                                </td>
                                <td class="input-column">
                                    <div><?php echo ADDRESS_PRE;?></div>
                                    <input  name="address" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-column"></td>
                            </tr>
                            <tr>
                                <td class="text-column"></td>
                                <td class="input-column">
                                    <input type="submit" onclick="return register()" value="注册"
                                           class="btn large" id="btn_register">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
</body>
</html>