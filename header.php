<?php require_once 'functions.php';?>

<div class="page-header block-bottom title1">

    <div class="middle-nav">
        <div class="middlenav-wrap clearfix">
            <div class="desire fright">
                <a class="ca-lightgrey home-page" href="index.php"><i class="icon i-home"></i><span>首页</span></a>
                <?php if(isLogin()) {?>
                <a class="ca-lightgrey check-order" href="center.php"><i class="icon i-search"></i><span>用户中心</span></a>
                <a class="ca-lightgrey check-order" href="password.php"><i class="icon i-search"></i><span>修改密码</span></a>
                <a class="ca-lightgrey check-order" href="logout.php"><i class="icon i-search"></i><span>退出</span></a>
                <?php } else {?>
                <a class="ca-lightgrey check-order" href="login.php"><i class="icon i-search"></i><span>登录或注册</span></a>
                <?php } ?>

            </div>
            <h1 class="logo fleft">
                <a title="" href="index.php">
                    <img alt="" src="static/images/logo.png"></a>
            </h1>
        </div>
    </div>
</div>