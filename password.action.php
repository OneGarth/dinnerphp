<?php
require_once 'functions.php';

if(!isLogin()) {
    alert('未登录或登录超时，请重新登录');
    goLogin();
    return;
}

$pwdOld = $_POST['pwdOld'];
$password = $_POST['password'];
if($pwdOld == '' || $password == '') {
    echo '请输入密码';
    return;
} else {
    if($password != $_POST['password2']) {
        echo '两次输入的密码不一致';
        return;
    }else if(strlen($password) < 6 || strlen($password) > 16) {
        echo '密码长度只能在6到16之间';
        return;
    } else if(!userLogin($_SESSION['userInfo']['email'], $pwdOld)) {
        echo '原密码不正确';
        return;
    } else {
        $r = updatePassword($_SESSION['userInfo']['_id'], $password);
        if($r >= 0) {
            echo 'ok';
        } else {
            echo '系统出错，请稍候重试';
        }
    }
}