<?php
require_once 'functions.php';

if(!isset($_POST['action'])) {
    if(!isset($_GET['action'])) {
        echo 'error: no action method.';
        return;
    } else {
        $action = $_GET['action'];
    }
} else {
    $action = $_POST['action'];
}
session_start();
// 登录
if($action == 'login') {

    if(strtolower(trim($_POST['captcha'])) != $_SESSION['captcha']) {
        echo '验证码输入错误';
        return;
    }

    if(!isEmail($_POST['email'])) {
        echo '请输入正确的邮箱地址';
        return;
    }

    if($_POST['password'] == '') {
        echo '请输入密码';
        return;
    }

    if(userLogin($_POST['email'], $_POST['password'])) {
        echo 'ok';
        return;
    } else {
        echo '邮箱或密码错误';
        return;
    }
}



// 注册
if($action == 'register') {

    if(!isEmail($_POST['email'])) {
        echo '请输入正确的邮箱地址';
        return;
    }

    if(userHas($_POST['email'])) {
        echo '此Email已注册';
        return;
    }

    if($_POST['name'] == '') {
        echo '名称不能为空';
        return;
    }

    if($_POST['phone'] == '') {
        echo '联系电话不能为空';
        return;
    }

    if($_POST['address'] == '') {
        echo '常用地址不能为空';
        return;
    }

    $password = $_POST['pwd'];
    if($password == '') {
            echo '请输入密码';
            return;
    } else {
        if($password != $_POST['pwd2']) {
            echo '两次输入的密码不一致';
            return;
        }else if(strlen($password) < 6 || strlen($password) > 16) {
            echo '密码长度只能在6到16之间';
            return;
        } else {
            $password = md5($password);
        }
    }

    $data = array(
        'email'=>$_POST['email'],
        'name'=>$_POST['name'],
        'password'=>$password,
        'phone'=>$_POST['phone'],
        'address'=>$_POST['address'],
        'register_time'=>date('Y-m-d H:i:s')
    );

    $result = userInsert($data);

    if($result >= 0) {
        echo 'ok';
    } else {
        echo '提交失败，请稍候重试';
    }
    return;
}

?>