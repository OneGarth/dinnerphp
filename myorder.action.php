<?php
require_once 'functions.php';

$result = 'Error';
$message = '';

if(!isLogin()) {
    $message = '未登录或登录超时，请重新登录！';
}

$order = orderGetById($_POST['id']);

if($order == null) {
    $message = '无此订单"';
}

if($order['user_id'] != $_SESSION['userInfo']['_id']) {
    $message = '这不是你的订单，你无权访问';
}

$r = orderCancel($_POST['id']);
if($r > 0) {
    $result = 'ok';
} else {
    $message = '出错，请稍候重试';
}

$arr = array(
    'result'=>$result,
    'message'=>$message
);

echo json_encode($arr);