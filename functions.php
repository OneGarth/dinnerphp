<?php
require_once 'include/db.php';

/**
 * 当前是否为营业时间
 */
function isShopTime() {
    global $db;
    $now = date('H:i');
    $r =  $db->select(TABLE_INFO, '*');
    $time1 = $r[0]['time1'];
    $time2 = $r[0]['time2'];
    if($time1 != '') {
        $time1 = explode('--', $time1);
        if($now >= $time1[0] && $now <= $time1[1]) {
            return true;
        }
    }

    if($time2 != '') {
        $time2 = explode('--', $time2);
        if($now >= $time2[0] && $now <= $time2[1]) {
            return true;
        }
    }

    return false;
}

/**
 * 获取店铺基本信息
 * @return array|bool|string
 */
function informationGet() {
    global $db;
    $r =  $db->select(TABLE_INFO, '*');
    return $r[0];
}

/**
 * 用户更改密码
 * @param $userId
 * @param $pwd
 * @return int
 */
function updatePassword($userId, $pwd) {
    global $db;
    return $db->update(TABLE_USER, array('password'=>md5($pwd)), array('_id'=>$userId));
}

/**
 * 用户取消订单
 * @param $orderId
 * @return int
 */
function orderCancel($orderId){
    global $db;
    return $db->update(TABLE_ORDER, array('status'=>3), array('order_id'=>$orderId));
}

/**
 * 返回用户所有订单
 * @param $userId
 * @return array|bool|string
 */
function orderGetByUser($userId) {
    global $db;
    return $db->select(TABLE_ORDER, '*', array('user_id'=>$userId));
}

/**
 * 根据订单ID获取订单
 * @param $id
 * @return null
 */
function orderGetById($id) {
    global $db;
    $result = $db->select(TABLE_ORDER, '*', array('order_id'=>$id));
    if($result != null && count($result) > 0) {
        return $result[0];
    } else {
        return null;
    }
}

/**
 * 插入订单
 * @param $data
 * @return array|string
 */
function orderInstert($data) {
    global $db;
    return $db->insert(TABLE_ORDER, $data);
}

/**
 * 用户登录
 * @param $email
 * @param $password
 * @return bool
 */
function userLogin($email, $password) {
    global $db;
    $result = $db->select(TABLE_USER, '*', array('AND'=>array('email'=>$email, 'password'=>md5($password))));
    if(count($result) > 0) {
        setLogin($result[0]);
        return true;
    } else {
        return false;
    }
}

/**
 * 新建用户
 */
function userInsert($data) {
    global $db;
    return $db->insert(TABLE_USER, $data);
}

function userHas($email) {
    global $db;
    $result = $db->select(TABLE_USER, '*', array('email'=>$email));
    if(count($result) > 0) {
        return true;
    } else {
        return false;
    }
}

function getDishById($id) {
    global $db;
    $result = $db->select(TABLE_DISH, '*', array('_id'=>$id));
    if($result != null && count($result) > 0) {
        return $result[0];
    } else {
        return null;
    }
}

function getDishByCategory($id) {
    global $db;
    return $db->select(TABLE_DISH, '*', array('AND'=>array('category_id'=>$id,'status'=>1)));
}

function getCategoryAll() {
    global $db;
    return $db->select(TABLE_CATEGORY, '*');
}

function isEmail($email) {
    $preg=preg_match("/\\w+([-+.']\\w+)*@\\w+\\.\\w+([-.]\\w+)*/",trim($email));
    if($preg) {
        return true;
    } else {
        return false;
    }
}

function isLogin() {
    session_start();
    if(isset($_SESSION['user']) && isset($_SESSION['userInfo'])
        && $_SESSION['user'] == true && $_SESSION['userInfo'] != null) {
        return true;
    } else {
        return false;
    }
}

function setLogin($user) {
	if(!isset($_SESSION)){
	    session_start();
	} 
	$_SESSION['user'] = true;
	$_SESSION['userInfo'] = $user;
}

function setLogout() {
	unset($_SESSION['userInfo']);
	unset($_SESSION['user']);
}

function logout() {
	unset($_SESSION['userInfo']);
	unset($_SESSION['user']);
	goLogin();
	exit();
}


function go($url) {
    echo '<script language="JavaScript">location.href="'. $url .'";</script>';
}

function goCenter() {
	echo '<script language="JavaScript">location.href="center.php";</script>';
}

function goLogin() {
	echo '<script language="JavaScript">location.href="login.php";</script>';
}

function showAlert($msg) {
	echo '<script language="JavaScript">alert("'.$msg.'");</script>';
}

function goBack() {
    echo '<script language="javascript">history.go(-1);</script>';
}

function alert($msg) {
    echo '<script language="javascript">alert("' . $msg . '");</script>';
}

?>