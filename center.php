<?php
require_once 'functions.php';

if(!isLogin()) {
    alert("登录超时或未登录，请重新登录");
    goLogin();
    exit;
}

$orders = orderGetByUser($_SESSION['userInfo']['_id']);

?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>用户中心</title>
    <script src="static/js/jquery-1.7.1.js"></script>
    <script src="static/js/jquery.validate.min.js"></script>
    <script src="static/js/jquery.gritter.min.js"></script>
    <link href="static/css/basic.css" rel="stylesheet">
    <link href="static/css/common.css" rel="stylesheet">
    <link href="static/css/restaurant.css" rel="stylesheet">
    <link href="static/css/jquery-ui.min.css" rel="stylesheet">
    <link href="static/css/jquery.ui.theme.css" rel="stylesheet">
    <link href="static/css/account_login.css" rel="stylesheet">
    <link href="static/css/jquery.gritter.css" rel="stylesheet">
</head>
<body>

<style>
    .mytable {
        width: 80%;
        border: solid #B2D460 1px;
    }
    .mytable tr{
        border-bottom: dotted #B2D460 1px;
    }
    .mytable tr td {
        vertical-align:middle;
        text-align:center;
        padding: 10px;
    }
    .mycenter {
        margin: 0 auto;
    }
    .textcenter {
        text-align: center;
    }
</style>

<?php include 'header.php'; ?>

<div class="page-wrap">
    <div class="inner-wrap">

        <div class="page-body block">
            <h2 class="title1 padding20 text-center">用户中心</h2>
            <div class="mycenter" style="padding: 20px;">

                <?php
                if($orders == null || count($orders) < 1) {
                    echo '<br><br><h2>暂无订单</h2><br><br>';
                } else {
                ?>
                    <table class="mytable mycenter">
                        <tr style="background-color: #B2D460;">
                            <td>订单号及时间</td>
                            <td>总价(元)</td>
                            <td>收餐人</td>
                            <td>状态</td>
                        </tr>
                        <?php
                        foreach($orders as $order) {
                            switch($order['status']) {
                                case 0:
                                    $status = '处理中';
                                    break;
                                case 1:
                                    $status = '配送中';
                                    break;
                                case 2:
                                    $status = '已完成';
                                    break;
                                case 3:
                                    $status = '无效订单';
                                    break;
                            }
                        ?>
                            <tr>
                                <td><?php echo '<a href="myorder.php?id=' . $order['order_id'] . '">'
                                        .  $order['order_id'] . '<br>' . $order['timestamp'] . '</a>';?>
                                </td>
                                <td><?php echo $order['price'];?></td>
                                <td><?php echo $order['user_name'];?></td>
                                <td><?php echo $status;?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
</body>
</html>