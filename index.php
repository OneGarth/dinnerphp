<?php
require_once 'functions.php';
$infor = informationGet();
$isShopTime = isShopTime();
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>在线订餐</title>
    <script src="static/js/jquery-1.7.1.js"></script>
    <script src="static/js/json2.min.js"></script>
    <script src="static/js/jquery.unobtrusive-ajax.min.js"></script>
    <script src="static/js/jquery.validate.min.js"></script>
    <script src="static/js/jquery.validate.unobtrusive.min.js"></script>
    <link href="static/css/basic.css" rel="stylesheet">
    <link href="static/css/common.css" rel="stylesheet">
    <link href="static/css/color.css" rel="stylesheet">
    <script src="static/js/jquery.cookie.js"></script>
    <script src="static/js/jquery.msgBox.js"></script>
    <script src="static/js/jquery.msgProcess.js"></script>
    <script src="static/js/jquery-ui-1.10.3.custom.min.js"></script>
    <link href="static/css/jquery-ui.min.css" rel="stylesheet">
    <link href="static/css/jquery.ui.theme.css" rel="stylesheet">
    <link href="static/css/restaurant.css" rel="stylesheet">
    <link href="static/css/ihover.css" rel="stylesheet">
    <script src="static/js/hallmenu.js"></script>

</head>
<body>
<?php include 'header.php';?>
<input id="shopTime" type="hidden" value="<?php echo $isShopTime?1:0;?>">

<div class="page-wrap">
    <div class="inner-wrap">


        <div id="category-menu-container">
            <div class="page-nav ui-tabs ui-widget ui-widget-content ui-corner-all" id="tabs">

                <div id="selectDish" aria-labelledby="ui-id-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">
                    <div class="menu-categories clearfix title1">

                        <?php
                        $category = getCategoryAll();
                        ?>
                        <h3>菜品分类：</h3>
                        <ul class="list clearfix">
                            <?php foreach($category as $cat):?>
                                <li>
                                    <a data-href="#category<?php echo $cat['_id'];?>" href="javascript:;"><?php echo $cat['name'];?> </a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <div id="content-list" class="content-list">

                    <?php foreach($category as $cat):?>
                    <div class="one-category-menus-container">
                        <a name="category<?php echo $cat['_id'];?>" id="category<?php echo $cat['_id'];?>" class="anchor"></a>
                        <h3 class="one-category-menu-title"><?php echo $cat['name'];?></h3>
                        <div class="one-category-menus">
                            <div class="one-category-menus clearfix" id="imgBoxList">
                                <?php
                                $dish = getDishByCategory($cat['_id']);
                                foreach($dish as $d) {
                                ?>
                                    <div class="imgproduct fleft">
                                        <div class="item image-food-item">
                                            <div class="pic">
                                                <a href="javascript:void(0)">
                                                    <!-- colored -->
                                                    <div class="ih-item square colored effect6 top_to_bottom">
                                                        <a href="#">
                                                            <div class="img"><img style="opacity: 1;" src="<?php echo URL_PIC . $d['image'];?>" alt="img"></div>
                                                            <div class="info">
                                                                <p><?php echo $d['detail'];?></p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <!-- end colored -->
<!--                                                    <img src="--><?php //echo URL_PIC . $d['image'];?><!--" style="opacity: 1;">-->
                                                </a>
                                            </div>
                                            <div class="name">
                                                <a href="javascript:void(0)" title="<?php echo $d['name'];?> "><?php echo $d['name'];?></a>
                                                <?php if($d['recommend']==1) echo '<span style="color: #ff0000;">[推荐]</span>';?>
                                            </div>
                                            <div class="detail">
                                                <span class="price">￥<i><?php echo $d['price'];?></i> / 份</span>
                                                <div class="addbtn">
                                                        <input type="button" class="add_btn">
                                                    <div class="invisible-data">
                                                        <p class="food-id"><?php echo $d['_id'];?></p>
                                                        <p class="food-name"><?php echo $d['name'];?></p>
                                                        <p class="food-price"><?php echo $d['price'];?></p>
                                                        <p class="food-min-order-count">1</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
</div>
                </div>
            </div>
        </div>

        <div id="right-bar">
                <div class="broadcaster block" style="margin-bottom: 10px;" id="broadcaster-bar">
                    <div class="title title1">
                        <h3>公告</h3>
                    </div>
                    <div class="content"><?php echo $infor['notice'];?></div>
                </div>

            <div class="broadcaster block">
                <div class="title title1">
                    <h3>联系</h3>
                </div>
                <div class="content">
                <p class="address"><b>商家地址：</b><?php echo $infor['address'];?></p>
                <p class="delivery-time"><b>营业时间：</b><?php echo $infor['time1'] . '，' . $infor['time2'];?></p>
                <p class="delivery-fee"><b>联系我们：</b><?php echo $infor['phone'];?></p>
                </div>
                <!-- <p class="delivery-area"></p>  -->
            </div>
            <div class="order-bar broadcaster block" id="order-bar">
                <div class="title title1">
                    <h3>购物车</h3>
                </div>
                <div class="content">
                <table id="deal_detail">
                    <tbody>
                        <tr>
                            <th style="width: 121px;" class="menu-name">菜品</th>
                            <th class="menu-count">份数</th>
                            <th class="menu-price">单价</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td style="width: 121px;" class="menu-name"></td>
                            <td class="menu-count"></td>
                            <td class="menu-price"></td>
                            <td class="menu-decrease"></td>
                        </tr>
                        <tr>
                            <td id="total-label" class="menu-name">总价</td>
                            <td colspan="2" id="total-price" class="menu-price">0</td>
                        </tr>
                    </tbody>
                </table>
                <form id="orderForm" method="post" action="order.php" novalidate="novalidate">
                    <div id="orderData"></div>
                    <input type="hidden" value="0" id="poi_id" name="poi_id">
                    <input type="submit" onclick="return checkDeal()" value="去下单" class="btn medium" id="subbtn" disabled="disabled">
                </form>
                    </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php include 'footer.php';?>
</body>
</html>