<?php
require_once 'functions.php';

$result = 'Error';
$message = '';

if(!isLogin()) {
    $message = '未登录或者登录超时，请重新登录！';
} else if(!isShopTime()) {
    $message = '未到营业时间，请稍候再提交';
} else if(!$_POST['check']) {
    $message =  '订单无效，请返回修改购物车菜品';
} else if($_POST['user_name'] == '') {
    $message =  '请输入联系人';
} else if($_POST['user_phone'] == '') {
    $message =  '请输入联系电话';
} else if($_POST['user_address'] == '') {
    $message =  '请输入地址';
} else {
    unset($_POST['btn_order']);
    unset($_POST['check']);
    $_POST['user_id'] = $_SESSION['userInfo']['_id'];
    $_POST['timestamp'] = date('Y-m-d H:i:s');
    $_POST['order_id'] = strtotime($_POST['timestamp']).rand(1000,9999);
    $_POST['status'] = 0;

    $r = orderInstert($_POST);
    if($r > 0) {
        $result = 'ok';
        $message = $_POST['order_id'];
    } else {
        $result = 'Error';
        $message = '出错了，请稍候重试';
    }
}

$arr = array(
    'result'=>$result,
    'message'=>$message
);

echo json_encode($arr);

?>