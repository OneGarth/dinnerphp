<?php
session_start();
$_SESSION['user'] = false;
$_SESSION['userInfo'] = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>正在跳转……</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="static/js/jquery-1.7.1.js"></script>
    <link href="static/css/basic.css" rel="stylesheet">
    <link href="static/css/common.css" rel="stylesheet">
    <link href="static/css/restaurant.css" rel="stylesheet">
</head>
<body>

<?php include 'header.php'; ?>

<div class="page-wrap">
    <div class="inner-wrap" style="width: 480px;">

        <div class="broadcaster block" style="margin-bottom: 10px;" id="broadcaster-bar">
            <div class="title title1">
                <h3 class="text-center">提示</h3>
            </div>
            <div class="content text-center" style="font-size: 16px;margin-bottom: 20px;">
                已退出，<span id="totalSecond">2</span>秒后自动跳转到主页……
            </div>
        </div>
    </div>

    <?php include 'footer.php'; ?>
</body>
<script language="javascript" type="text/javascript">
    <!--
    var second = document.getElementById('totalSecond').innerText;

    setInterval("redirect()", 1000); //每1秒钟调用redirect()方法一次
    setTimeout(function(){location.href = 'index.php';}, 3000);
    function redirect() {
        if (second < 0) {
//            location.href = 'index.php';
        } else {
            document.getElementById('totalSecond').innerText = second--;
        }
    }
    -->
</script>
</html>