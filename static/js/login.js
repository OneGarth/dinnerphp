

function login() {
    $('#btn_login').html = '提交中……';
    $.ajax({
        //this is the php file that processes the data and send mail
        url: "login.action.php?action=login",
        //GET method is used
        type: "POST",
        //pass the data
        data: $('#loginform').serialize(),
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html == 'ok') {
                location.href="index.php";
                $.gritter.add({
                    title:	'提示',
                    text:	'登录成功，跳转中……',
                    time: 5000,
                    sticky: false
                });
            } else {
                document.getElementById('captcha_img').src = 'include/captcha.php?'+Math.random();
                $.gritter.add({
                    title:	'提示',
                    text:	html,
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            document.getElementById('captcha_img').src = 'include/captcha.php?'+Math.random();
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {
            $('#btn_login').html = '登录';
        }
    });

    return false;
}



function register() {
    $('#btn_register').html = '注册中……';
    $.ajax({
        //this is the php file that processes the data and send mail
        url: "login.action.php?action=register",
        //GET method is used
        type: "POST",
        //pass the data
        data: $('#registerform').serialize(),
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html == 'ok') {
                $.gritter.add({
                    title:	'提示',
                    text:	'注册成功，请登录……',
                    time: 5000,
                    sticky: false
                });
            } else {
                $.gritter.add({
                    title:	'提示',
                    text:	html,
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {
            $('#btn_register').html = '注册';
        }
    });

    return false;
}