﻿
var status;
var deal = [];
$(document).ready(function () {
    /* 页面上展开和收起按钮 */
    $('.description .open').bind('click', function (e) {
        e.stopPropagation();
        $(this).parent().hide();
        $(this).parent().next().show();
    }).hover(function () {
        $(this).find('.icon').addClass('i-arrowdown-hover');
        $(this).css('color', '#ed9400');
    }, function () {
        $(this).find('.icon').removeClass('i-arrowdown-hover');
        $(this).css('color', '#aaaaaa');
    });
    $('.description .close').bind('click', function (e) {
        e.stopPropagation();
        $(this).parent().hide();
        $(this).parent().prev().show();
    }).hover(function () {
        $(this).find('.icon').addClass('i-arrowup-hover');
        $(this).css('color', '#ed9400');
    }, function () {
        $(this).find('.icon').removeClass('i-arrowup-hover');
        $(this).css('color', '#aaaaaa');
    });
    $('.product .description').bind('click', function (e) {
        e.stopPropagation();
        var $descfull = $(this).find('.desc-full'), $descbrief = $(this).find('.desc-brief');
        if ($descfull.css('display') == 'none') {
            $descbrief.find('a').trigger('click');
        } else {
            $descfull.find('a').trigger('click');
        }
    });
    $('.menu-categories li').click(function (e) {
        e.preventDefault();
        $('.menu-categories li').removeClass('active');
        $(this).addClass('active');
        var hashValue = $(this).find('a').data('href');
        var $hash = $(hashValue);
        var scrollTop = $hash.offset().top;
        $(window).scrollTop(scrollTop - $('.menu-categories').height() - 25);
    });
    // 点击图片下面的文字时直接增加菜品
    $('.image-food-item .operation a').click(function (event) {

        var $elemInvisibleData = $(this).parent().next();
        var foodID = $elemInvisibleData.children('.food-id').text();
        var foodName = $elemInvisibleData.children('.food-name').text();
        var foodPrice = $elemInvisibleData.children('.food-price').text();
        var foodMin = $elemInvisibleData.children('.food-min-order-count').text();
        addFood({ id: foodID, name: foodName, price: foodPrice, sum: 1, min: foodMin });
        event.stopPropagation();
    });
    // 窗口滚动的时候控制悬浮效果。
    var $orderBar = $('#order-bar'), $category = $('.menu-categories');
    var orderBarHook = $orderBar.offset().top, categoryHook = $category.offset().top;
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop < categoryHook) {
            $orderBar.removeClass('order-bar-fixed');
            $category.removeClass('menu-categories-fixed');
            $('#category-menu-container .content-list').removeAttr('style');
        } else if (categoryHook <= scrollTop && scrollTop <= orderBarHook) {
            $orderBar.removeClass('order-bar-fixed');
            $category.addClass('menu-categories-fixed');
            $('#category-menu-container .content-list').css('margin-top', $category.height() + 30 + 'px');
        } else if (scrollTop > orderBarHook) {
            $orderBar.addClass('order-bar-fixed');
            $category.addClass('menu-categories-fixed');
            $('#category-menu-container .content-list').css('margin-top', $category.height() + 30 + 'px');
        }
    });
    // end
    //选择整一行的菜单的时，增加样式
    $('.image-food-item').hover(function () {
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
    });

    var poiid = '840';

    if ($.cookie('poiid') != null && $.cookie('poiid') != '' && $.cookie('poiid') != poiid) {
        clearPrePoiDeal();
    }
    $.cookie('poiid', poiid, { path: '/' });

    var min_price = '0';
    $.cookie('wmp', min_price, { path: '/' });
    /** 从cookie中初始化购物车 */
    var c_p = $.cookie('poiid');
    var c_c = $.cookie('w_c');
    if (typeof (c_p) == 'undefined'
    || c_p == null
    || typeof (c_c) == 'undefined'
    || c_c == null) {
        ;
    } else {
        remoteInitCart();
    }
    /** 设置店的状态 */
    status = $("#openstate").val();
    /** 初始设置按钮 */
    if (status == 0) {
        $('#subbtn').val('休息中，暂不能配送');
        $('#subbtn').attr("disabled", "disabled");
    } else {
        if (min_price > 0.000001) {
            $('#subbtn').val('还差' + min_price + '元起送');
        } else {
            $('#subbtn').val('去下单');
        }
        $('#subbtn').attr("disabled", "disabled");
    }
    $('div.product').hover(
    function () {
        $(this).addClass('over');
    },
    function () {
        $(this).removeClass('over');
    }
    );
    $('div.product').click(function () {
        $(this).children('img.over').trigger("click");
    });
    $('div.product > img.over').click(function (event) {
        var $elemInvisibleData = $(this).parent().children('div.invisible-data');
        var foodID = $elemInvisibleData.children('.food-id').text();
        var foodName = $elemInvisibleData.children('.food-name').text();
        var foodPrice = $elemInvisibleData.children('.food-price').text();
        var foodMin = $elemInvisibleData.children('.food-min-order-count').text();
        addFood({ id: foodID, name: foodName, price: foodPrice, sum: 1, min: foodMin });
        event.stopPropagation();
    });
    //图片按钮
    $('div.imgproduct').click(function () {
        $(this).children('div.item').children('div.detail').children('div.addbtn').children('input.add_btn').trigger("click");
    });
    $('div.imgproduct >div.item>div.detail>div.addbtn>input.add_btn').click(function (event) {
        var $elemInvisibleData = $(this).parent().children('div.invisible-data');
        var foodID = $elemInvisibleData.children('.food-id').text();
        var foodName = $elemInvisibleData.children('.food-name').text();
        var foodPrice = $elemInvisibleData.children('.food-price').text();
        var foodMin = $elemInvisibleData.children('.food-min-order-count').text();
        addFood({ id: foodID, name: foodName, price: foodPrice, sum: 1, min: foodMin });
        event.stopPropagation();
    });
    /*结束 图片按钮*/

    initCart();
});
// 远程验证购物车, 获得购物车数据。
function remoteInitCart() {
    //$.get("/cart/recart?" + Math.random(),
    //{},
    //function (data, status) {
    //    if (status == 'success' && 1 == data['code']) {
    //        var cart = eval(data['cart']);
    //        deal = cart;
    //        //initCart();
    //        freshCart();
    //    }
    //});
}
//food:{id:,sum:,price:,name:}


function addFood(food) {
    var have = false;;
    for (f in deal) {
        if (deal[f].id == food.id) {
            deal[f].sum += 1;
            have = true;
            break;
        }
    }
    if (!have) {
        food.sum = Number(food.min);
        deal.push(food);
    }
    freshCart();
    return false;
}
function addFoodWithID(foodid) {
    addFood({ id: foodid });
}
function decreaseFoodWithID(foodid) {
    var newdeal = [];
    for (f in deal) {
        if (deal[f].id == foodid) {
            if (deal[f].sum > 1) {
                deal[f].sum -= 1;
                if (deal[f].sum < deal[f].min) {
                    continue;
                }
            } else {
                continue;
            }
        }
        newdeal.push(deal[f]);
    }
    deal = newdeal;
    freshCart();
}
function countChars(str, len, flag, html_replace) {
    if (str) {
        var strLen = str.replace(/[\u4e00-\u9fa5\s]/g, '**').length, newStr = [], totalCount = 0;
        if (html_replace) {
            var $elem = $('<div></div>').html(str);
            str = $elem.text();
            $elem = null;
        }
        if (strLen <= len) {
            return str;
        } else {
            for (var i = 0; i < strLen; i++) {
                var nowValue = str.charAt(i);
                if (/[^\x00-\xff]/.test(nowValue)) {
                    totalCount += 2;
                } else {
                    totalCount += 1;
                }
                newStr.push(nowValue);
                if (totalCount >= len) {
                    break;
                }
            }
            if (flag) {
                return newStr.join('');
            } else {
                return newStr.join('') + '...';
            }
        }
    } else {
        return '';
    }
};
function freshCart() {
    var sum = 0;
    var detail = '<tr><th class="menu-name" style="width: 121px;">菜品</th><th class="menu-count">份数</th><th class="menu-price">单价</th><th class="menu-decrease"></th></tr>';
    var w_c = '';
    var formhtml = '';
    for (f in deal) {
        sum += deal[f].sum * deal[f].price;
        detail += '<tr><td class="menu-name" style="width: 121px;"><span title="' + deal[f].name + '">' + countChars(deal[f].name, 12) + '</span></td><td class="menu-count">';
        detail += '<img class="cart-action" onclick="decreaseFoodWithID(' + deal[f].id + ');" src="static/images/icon-minus.gif" width="20" height="20" /><span>' + deal[f].sum + '</span>';
        detail += '<img class="cart-action" onclick="addFoodWithID(' + deal[f].id + ');" src="static/images/icon-plus.gif" width="20" height="20" /></td><td class="menu-price">' + deal[f].price + '</td>';
        detail += '<td class="menu-decrease"></td></tr>';
        //w_c += deal[f].id + ',' + deal[f].sum + '|';
        w_c += deal[f].id + ',' + deal[f].price + ',' + deal[f].sum + ',' + deal[f].sum * deal[f].price + '|';

        formhtml += '<input type="hidden" name="id[]" value="' + deal[f].id + '" />';
        formhtml += '<input type="hidden" name="name[]" value="' + deal[f].name + '" />';
        formhtml += '<input type="hidden" name="count[]" value="' + deal[f].sum + '" />';
    }
    detail += '<tr><td class="menu-name" id="total-label">总价</td><td class="menu-price" id="total-price" colspan="2">' + sum + '</td></tr>'
    $('#deal_detail').html(detail);
    $('#orderData').html(formhtml);
    // new
    $.cookie('w_c', w_c, { path: '/' });
    // 将购物车信息放入cookie中
    $.cookie('deal', JSON.stringify(deal));
    /** 设置去下单按钮 */
    if (status == 0) {
        $('#subbtn').val('休息中，暂不能配送');
        $('#subbtn').attr("disabled", "disabled");
    } else {
        var min_price = $.cookie('wmp');
        if ((sum - min_price) < -0.000001) {
            $('#subbtn').val('还差' + (min_price - sum) + '元起送');
            $('#subbtn').attr("disabled", "disabled");
        } else {
            $('#subbtn').val('去下单');
            $('#subbtn').removeAttr("disabled");
        }
        if (deal.length == 0 && min_price < 0.000001) {
            $('#subbtn').val('去下单');
            $('#subbtn').attr("disabled", "disabled");
        }
    }
}
function checkDeal() {
    if (deal.length == 0) {
        return false;
    }
    var shopTime = $('#shopTime').val();
    if(shopTime != 1) {
        alert('未到营业时间，暂不能下单');
        return false;
    }
    return true;
}

function clearPrePoiDeal() {
    $.cookie('w_c', '', { path: '/' });
}
/** 从cookie中初始化购物车 */
function initCart() {
    if($.cookie('deal')) {
        deal = JSON.parse($.cookie('deal'));
    } else {
        deal = [];
    }
    freshCart();
}