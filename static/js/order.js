

function order() {
    $('#btn_order').html = '提交中……';
    $.ajax({
        dataType: "json",
        //this is the php file that processes the data and send mail
        url: "order.action.php",
        //GET method is used
        type: "POST",
        //pass the data
        data: $('#orderform').serialize(),
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html.result == 'ok') {
                // 下订单成功，清除购物车
                $.cookie('deal', null);
                // 跳转到订单详情
                location.href="myorder.php?id=" + html.message;
                $.gritter.add({
                    title:	'提示',
                    text:	'下单成功，跳转中……',
                    time: 5000,
                    sticky: false
                });
            } else {
                $.gritter.add({
                    title:	'提示',
                    text:	html.message,
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {
            $('#btn_order').html = '提交';
        }
    });

    return false;
}

