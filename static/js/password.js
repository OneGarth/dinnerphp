
function editPassword() {
    $('#btn_login').html = '提交中……';
    $.ajax({
        //this is the php file that processes the data and send mail
        url: "password.action.php",
        //GET method is used
        type: "POST",
        //pass the data
        data: $('#passwordform').serialize(),
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html == 'ok') {
                alert('修改成功，请重新登录');
                location.href="login.php";
            } else {
                $.gritter.add({
                    title:	'提示',
                    text:	html,
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {
            $('#btn_login').html = '提交';
        }
    });

    return false;
}
