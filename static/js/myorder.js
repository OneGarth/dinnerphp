
function cancelOrder(id) {
    $('#cancelbtn').html = '提交中……';
    $.ajax({
        dataType: "json",
        //this is the php file that processes the data and send mail
        url: "myorder.action.php",
        //GET method is used
        type: "POST",
        //pass the data
        data: "id=" + id,
        //Do not cache the page
        cache: false,
        //success
        success: function(html) {
            if(html.result == 'ok') {
                $.gritter.add({
                    title:	'提示',
                    text:	'操作成功',
                    time: 3000,
                    sticky: false
                });
                $('#cancelbtn').fadeOut('fast');
                location.reload();
            } else {
                $.gritter.add({
                    title:	'提示',
                    text:	html.message,
                    time: 3000,
                    sticky: false
                });
            }
        },

        error: function() {
            $.gritter.add({
                title:	'提示',
                text:	'系统有误，请联系系统管理员',
                time: 3000,
                sticky: false
            });
        },

        complete: function() {
        }
    });
}