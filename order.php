<?php
require_once 'functions.php';

if (!isLogin()) {
    goLogin();
    exit;
}

if(!isShopTime()) {
    alert('未到营业时间，请稍候提交');
    exit;
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>下单</title>
    <script src="static/js/jquery-1.7.1.js"></script>
    <script src="static/js/jquery.cookie.js"></script>
    <script src="static/js/jquery.unobtrusive-ajax.min.js"></script>
    <script src="static/js/jquery.validate.min.js"></script>
    <script src="static/js/jquery.validate.unobtrusive.min.js"></script>
    <script src="static/js/jquery.gritter.min.js"></script>
    <script src="static/js/order.js"></script>

    <link href="static/css/basic.css" rel="stylesheet"/>
    <link href="static/css/common.css" rel="stylesheet"/>
    <link href="static/css/color.css" rel="stylesheet"/>
    <link href="static/css/jquery.gritter.css" rel="stylesheet">
    <link href="static/css/order_preview.css" rel="stylesheet"/>
    <link href="static/css/account_login.css" rel="stylesheet"/>

</head>
<body>

<?php
include 'header.php';
$ids = $_POST['id'];
$counts = $_POST['count'];
?>

<div class="page-wrap">
    <div class="inner-wrap">
        <div id="body-wrap">

            <div id="order-wrap">
                <div class="title1 text-center" style="padding-top: 10px;"><h3>确认订单</h3></div>
                <div class="padding20">
                <div id="confirm-order-left">
                    <div id="order-restaurant">
                        <h2>我的订单</h2>
                    </div>
                    <div id="confirm-order-food-list" class="order-food-list">
                        <table>
                            <tbody>
                            <tr>
                                <th class="menu-name">菜品</th>
                                <th class="menu-price">单价</th>
                                <th class="menu-count">份数</th>
                                <th class="menu-subtotal">金额</th>
                            </tr>
                            <?php
                            $content = array();
                            $orderValid = true;
                            $allSum = 0;
                            for ($i = 0; $i < count($ids); $i++):
                                $dish = getDishById($ids[$i]);
                                if ($dish == null)
                                    continue;
                                if ((int)$dish['status'] != 1) {
                                    $temp = '目前此菜品不可购买，请重新选择';
                                    $count = $counts[$i].'<br><span color="red">'.$temp.'</span>';
                                    $sum = 0;
                                    $orderValid = false;
                                } else {
                                    $count = $counts[$i];
                                    $sum = $count * (float)$dish['price'];
                                    $allSum += $sum;
                                    $contentUnit = array();
                                    $contentUnit['id'] = $ids[$i];
                                    $contentUnit['name'] = $dish['name'];
                                    $contentUnit['price'] = $dish['price'];
                                    $contentUnit['count'] = $count;
                                    $content[$i] = $contentUnit;
                                }

                                ?>
                                <tr>
                                    <td class="menu-name"><?php echo $dish['name']; ?></td>
                                    <td class="menu-price">￥<?php echo $dish['price']; ?></td>
                                    <td class="menu-count"><?php echo $count;?></td>
                                    <td class="menu-subtotal">￥<?php echo $sum; ?></td>
                                </tr>
                            <?php endfor; ?>
                            <tr class="total">
                                <td class="menu-name total-label"><b>合计</b></td>
                                <td class="menu-price"></td>
                                <td class="menu-count"></td>
                                <td class="menu-subtotal total-price">￥<?php echo $allSum; ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="confirm-order-right">
                    <?php if($orderValid) { ?>
                    <form action="order.action.php" id="orderform" method="post">
                        <input type="hidden" name="check" value="<?php echo $orderValid;?>">
                        <input type="hidden" name="price" value="<?php echo $allSum;?>">
                        <input type="hidden" name="content" value='<?php echo json_encode($content);?>'>
                        <div id="confirm-order-address">
                            <h2>送餐信息</h2>
                            <table>
                                <tbody>
                                <tr>
                                    <td class="title"><span style="color: red;">*&nbsp;</span>联系人</td>
                                    <td>
                                        <input class="text" id="Contact" name="user_name" type="text"
                                               value="<?php echo $_SESSION['userInfo']['name']; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="title"><span style="color: red;">*&nbsp;</span>联系电话</td>
                                    <td>
                                        <input class="text" data-val="true" data-val-required="请填写联系电话"
                                               id="Phone" name="user_phone" placeholder="" type="text"
                                               value="<?php echo $_SESSION['userInfo']['phone']; ?>"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="title"><span style="color: red;">*&nbsp;</span>送餐地址</td>
                                    <td>
                                        <div><?php echo ADDRESS_PRE; ?></div>
                                        <input class="text" data-val="true" data-val-required="请填写送餐地址"
                                               id="Address" name="user_address" placeholder="" type="text"
                                               value="<?php echo $_SESSION['userInfo']['address']; ?>"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-column"></td>
                                </tr>
                                <tr>
                                    <td class="text-column"></td>
                                    <td class="input-column">
                                        <input type="submit" onclick="return order()" value="提交"
                                               class="btn large" id="btn_order" name="btn_order">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <?php } else { ?>
                    <div class="text-center"><h2>菜品库存不足，请返回修改购物车菜品数量</h2></div>
                    <?php } ?>
                </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>


<?php include 'footer.php'; ?>

</body>
</html>
