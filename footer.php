<div class="page-footer">
    <div class="footer-wrap">
        <div class="column fleft update">
            <div class="title">关注我们</div>
            <ul>
                <li><a target="_blank" class="ca-darkgrey" href="#">新浪微博</a></li>
                <li><a target="_blank" class="ca-darkgrey" href="#">腾讯微博</a></li>
            </ul>
        </div>
        <div class="column fleft corp">
            <div class="title">商务合作</div>
            <ul>
                <li><a target="_blank" class="ca-darkgrey" href="#">提供服务</a></li>

            </ul>
        </div>
        <div class="column fleft info">
            <div class="title">商家信息</div>
            <ul>
                <li><a target="_blank" class="ca-darkgrey" href="#">关于我们</a></li>
                <li><a target="_blank" class="ca-darkgrey" href="#">加入我们</a></li>
            </ul>
        </div>
        <div class="clear"></div>
        <div class="copyright">
            ©Copyright ©2014-2015 All Rights Reserved
        </div>
    </div>
</div>